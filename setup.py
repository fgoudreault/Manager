from setuptools import setup
import warnings


with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(name="manager",
      description="DMFT calculations project manager.",
      install_requires=requirements,
      )

# MySQL Python connector
try:
    import mysql.connector  # noqa
except ImportError:
    print("\n\n################################################\n\n")
    warnings.warn("\n No MySQL python connector found.\n Please install"
                  " one manually.\n We suggest the development connector"
                  " given by MySQL. It can be found here:\n\n"
                  " https://dev.mysql.com/downloads/connector/python/\n\n"
                  " just download the source code and issue:\n"
                  " 'python setup.py install' \nfor easy install compatible"
                  " with conda.")
    print("\n\n################################################\n\n")
