from .kptmesh import KptMesh
from .hamiltonian import Hk
from .gap import Gap
import numpy as np


class Bogoliubov:
    def __init__(self, hk, gap, mesh=None):
        if not isinstance(hk, Hk):
            raise TypeError("The given hamiltonian needs to be a Hk object.")
        if not isinstance(gap, Gap):
            raise TypeError("The given gap function needs to be a Gap object.")
        if mesh is not None and not isinstance(mesh, KptMesh):
            raise TypeError("The given mesh needs to be a KptMesh object.")
        if hk.dim != gap.ndim:
            raise ValueError("Hamiltonian and gap functions have different "
                             "dimensionality.")

        self.hk = hk
        self.gap = gap

        self.ndim = 2*hk.dim

        if mesh is None:
            self.mesh = hk.mesh
        else:
            self.mesh = mesh

        self.bogoliubov = np.zeros((self.mesh.nkpt, self.ndim, self.ndim))
        self.eigs = np.zeros((self.mesh.nkpt, self.ndim))

    def construct_diagonalize(self):
        if self.mesh.n != self.hk.mesh.n:
            # Interpolate hk
            raise NotImplementedError("Interpolation of the hamiltonian not "
                                      "implemented yet.")

        if self.mesh.n != self.gap.mesh.n:
            # Interpolate gap
            gap = self.gap.interpolate(self.mesh)
        else:
            gap = self.gap

        gap_dag = np.transpose(np.conj(gap))

        self[:] = np.block([[self.hk[:], self.gap[:]],
                            [gap_dag[:], -self.hk[:]]])

        self.eigs[:] = np.linalg.eigvals(self[:])

    def __getitem__(self, key):
        return self.bogoliubov[key]
