from .physobj import PhysObj
from .kptmesh import KptMesh
import numpy as np
import itertools as it
import scipy.linalg as ln
from itertools import product
from scipy.interpolate import RegularGridInterpolator as GridInt


class Gap:
    def __init__(self, ndim, mesh, nfreq):
        print("Gap.__init__: Initializing Gap function object.")

        if not isinstance(mesh, KptMesh):
            raise TypeError("ERROR in Gap.__init__: mesh has to be "
                            "a KptMesh object.")
        elif not mesh.nkpt:
            raise ValueError("ERROR in Gap.__init__: mesh has no kpoints.")
        elif not ndim:
            raise ValueError("ERROR in Gap.__init__: ndim cannot be 0.")
        elif not nfreq:
            raise ValueError("ERROR in Gap.__init__: nfreq cannot be 0.")
        elif nfreq % 2:
            raise ValueError("Gap must have an even number of fermionic "
                             "frequencies, since it's defined on both "
                             "positive and negative frequencies.")

        self.ndim = ndim
        self.mesh = mesh
        self.nfreq = nfreq

        self.Re = PhysObj(ndim, mesh.nkpt, nfreq, linear=True)
        self.Im = PhysObj(ndim, mesh.nkpt, nfreq, linear=True)

    def TestAllSym(self, basis="Orb", kset=None, precision=1e-10):
        r"""Crystal symmetry:
           From a gap objects that is read, with a definitive kptmesh, apply
           all the crystal symmetries such as
                r"\Delta(gk) = G * \Delta(k) * G^{-1}"
           where g a the symmetry matrix on k point defined by the space group
           and G is the corresponding transformation matrix in the choosen
           basis. Returns all broken symmetries.

           Odd-frequency:
           Check if even or odd under frequenecy by comparing
           "\Delta(\vec{k}, -w)_{l_2, l_1} ="
           " \pm \Delta(\vec{k}, w)_{l_1, l_2}."
           Broken-symmetry is written as 'w'.

        Parameters
        ----------
        basis : str
                Defines the space where symmetries are checked. For now, the
                main purpose is in the orbital basis.
        kset : it.product
               Set of k points to check. If None, check all.
        precision : float
                    Precision of the comparaison.
        """

        if not self.mesh.nkpt:
            raise ValueError("Mesh not loaded.")
        # Symmetries enconded in the space group
        gs = self.mesh.sgroup.gs
        if basis == "Orb":
            # We need to tell the mesh what is the orbital space to act on
            # That is because the matrices in orbital space are non-trivial
            if not len(self.mesh.sgroup.orbs) == self.ndim:
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap functions.")
        else:
            raise NotImplementedError("The TestCrystalSym function is not "
                                      "implemented for this basis yet.")

        # Construct full gap object, in dims of (nkpt, nfreq, ndim, ndim)
        Gap = self.Re.Obj + 1j*self.Im.Obj

        # Stats information
        broken_sym = []
        err = 0
        nk = 0

        # Writing for debug
        np.set_printoptions(linewidth=10000, threshold=10000, precision=5)

        if kset is None:
            kset = it.product(range(2*self.mesh.n[0]), range(2*self.mesh.n[1]),
                              range(2*self.mesh.n[2]))

        # Check each points
        # TODO: Make it more efficient?
        for nx, ny, nz in kset:
            # Gap is unidimensional in kpts, so keep track of index
            m1 = self.mesh[nx, ny, nz]
            # Only check at encoded points (just be all the FBZ)
            if m1 != -1:
                nk += 1
                err_k = 0
                # Symmetries are done around the center, hence k here
                n1 = np.transpose(np.matrix(self.mesh.npts[m1]))
                k1 = n1-self.mesh.N0
                Gap1 = Gap[m1]
                # Crystal symmetries
                # ------------------
                for g in gs:
                    # Associate symmetry matrix for orbital representation
                    G = self.mesh.sgroup.GtoOrb(g)
                    # Symmetric point information
                    k2 = self.mesh.sgroup.GtoMatrix(g).dot(k1)
                    n2 = k2+self.mesh.N0
                    n2 = self.mesh.translate_in_FBZ([n2.item(0), n2.item(1),
                                                     n2.item(2)])
                    n2x = int(round(n2[0]))
                    n2y = int(round(n2[1]))
                    n2z = int(round(n2[2]))
                    m2 = self.mesh[n2x, n2y, n2z]
                    Gap2 = Gap[m2]
                    # Check all frequencies
                    for w in range(self.nfreq):
                        '''
                        print("------------")
                        print("Sym: %s; w: %d" % (g, w))
                        print("k1: ", np.transpose(k1))
                        print("k2: ", np.transpose(k2))
                        print("gk: \n", self.mesh.sgroup.GtoMatrix(g))
                        print("n1: ", np.transpose(n1), "; mesh: ", m1,
                              "\nn2: ", np.transpose(n2), "; mesh: ", m2)
                        '''
                        G2 = G.dot(Gap2[w]).dot(ln.inv(G))
                        # Detects symmetry breaking
                        if (np.amax(np.absolute(Gap1[w] - G2)) > precision):
                            err_k = 1
                            # Debugging prints
                            '''
                            print("Broken sym")
                            print(self.mesh.fromNgetM(n2))
                            print(Gap1[w])
                            print(G2)
                            print(G)
                            print(Gap2[w])
                            print(ln.inv(G))
                            '''
                            # Add broken symmetry if new
                            if g not in broken_sym:
                                broken_sym.append(g)
                if err_k:
                    err += 1

                odd_freq = None
                # Odd-frequency check
                # -------------------
                for l1, l2 in it.product(range(self.ndim), range(self.ndim)):
                    for w in range(round(self.nfreq/2)):
                        w2 = self.nfreq - 1 - w

                        diff_gap = Gap[m1, w, l1, l2] - Gap[m1, w2, l2, l1]
                        sum_gap = Gap[m1, w, l1, l2] + Gap[m1, w2, l2, l1]

                        if np.amax(np.absolute(diff_gap)) < precision:
                            iseven = True
                        else:
                            iseven = False

                        if np.amax(np.absolute(sum_gap)) < precision:
                            isodd = True
                        else:
                            isodd = False

                        if iseven and isodd:
                            break
                        elif iseven:
                            if odd_freq is None:
                                odd_freq = "even-w"
                            elif odd_freq == "odd-w":
                                raise ValueError("Inconsistent odd-frequency "
                                                 "symmetry! Check and "
                                                 "document.")
                        elif isodd:
                            if odd_freq is None:
                                odd_freq = "odd-w"
                            elif odd_freq == "even-w":
                                raise ValueError("Inconsistent odd-frequency "
                                                 "symmetry! Check and "
                                                 "document.")

                        if not iseven and not isodd:
                            raise ValueError("Perhaps degeneracy. Document.")

                if odd_freq is not None:
                    if odd_freq in broken_sym:
                        pass
                    elif ("even-w" not in broken_sym and "odd-w" not in
                          broken_sym):
                        broken_sym.append(odd_freq)
                    else:
                        raise ValueError("Inconsistent odd-frequency for "
                                         "different k points.")

        # Print stats
        print("Number of k points with mesh != -1: %d" % nk)
        print("Number of k pts with error: %d" % err)
        # Return broken syms
        return broken_sym

    def interpolate(self, mesh2):
        """From a gap function defined on a given mesh, interpolate to a finer
           mesh. Returns another object of type Gap.

        Parameters
        ----------
        mesh2 : KptMesh
                Mesh for the interpolated gap object.
        """
        m = self.mesh
        mgap = np.zeros((2*m.n[0]+1, 2*m.n[1]+1, 2*m.n[2]+1, self.nfreq,
                         self.ndim, self.ndim), dtype=complex)

        q_set = product(range(2*m.n[0]+1), range(2*m.n[1]+1),
                        range(2*m.n[2]+1))
        for qx, qy, qz in q_set:
            q = m.translate_in_FBZ([qx-1, qy-1, qz-1])
            mgap[qx, qy, qz] = self[m[q[0], q[1], q[2]], :]

        interpol = np.zeros((self.nfreq, self.ndim, self.ndim),
                            dtype=GridInt)
        for f in range(self.nfreq):
            for l1, l2 in product(range(self.ndim), range(self.ndim)):
                linx = np.linspace(-1, 2*m.n[0]-1, 2*m.n[0]+1)
                liny = np.linspace(-1, 2*m.n[1]-1, 2*m.n[1]+1)
                linz = np.linspace(-1, 2*m.n[2]-1, 2*m.n[2]+1)
                interpol[f, l1, l2] = GridInt((linx, liny, linz),
                                              mgap[:, :, :, f, l1, l2])

        int_mgap = np.zeros((2*mesh2.n[0], 2*mesh2.n[1], 2*mesh2.n[2],
                             self.nfreq, self.ndim, self.ndim), dtype=complex)

        q_set2 = product(range(2*mesh2.n[0]), range(2*mesh2.n[1]),
                         range(2*mesh2.n[2]))
        gap2 = np.zeros((mesh2.n[0]*mesh2.n[1]*mesh2.n[2]*2, self.nfreq,
                         self.ndim, self.ndim), dtype=complex)

        numq = 8*mesh2.n[0]*mesh2.n[1]*mesh2.n[2]
        numdone = 0
        prct = 0

        i = 0
        for qx, qy, qz in q_set2:
            if numdone / numq * 100 >= prct:
                print("[%d/100]" % prct)
                prct += 1
            numdone += 1
            q2x = 1.*(qx-mesh2.N0.item(0))*m.n[0]/mesh2.n[0]+m.N0.item(0)
            q2y = 1.*(qy-mesh2.N0.item(1))*m.n[1]/mesh2.n[1]+m.N0.item(1)
            q2z = 1.*(qz-mesh2.N0.item(2))*m.n[2]/mesh2.n[2]+m.N0.item(2)
            if mesh2.in_FBZ(mesh2.fromNgetM([qx, qy, qz])):
                mesh2[qx, qy, qz] = i
                mesh2.npts.append([qx, qy, qz])
                for l1, l2 in product(range(self.ndim), range(self.ndim)):
                    for f in range(self.nfreq):
                        v = interpol[f, l1, l2](np.array([q2x, q2y, q2z]))
                        gap2[i, f, l1, l2] = v
                        if f == 0:
                            int_mgap[qx, qy, qz, 0, l1, l2] = v
                i += 1
        mesh2.nkpt = len(mesh2.npts)

        Gap2 = Gap(self.ndim, mesh2, self.nfreq)
        Gap2.Re.Obj = gap2.real
        Gap2.Im.Obj = gap2.imag
        return Gap2

    def changeBasis(self, proj, pmesh):
        """The gap function is obtained in the orbital basis, but to calculate
           physical quantities from it, we need it first in the band basis. By
           giving a set of projectors, probably defined on a coarser grid, this
           this function calculates and returns the gap in the band basis.

        Parameters
        ----------
        proj : Projectors
               Set of projectors allowing the change between orbital and band
               basis.
        pmesh : KptMesh
                Mesh on which the projectors are defined. Because these are
                calculated for the BareChi, it is probably a much coarser mesh
                than the gap, which needs to be relatively small for the
                Eliashberg solving. This mesh needs to be a submesh of the gap
                mesh, but it has to be anyway.
        """
        # Proj can only work for one atom now, easy to do.
        if proj.natom > 1:
            raise ValueError("Only works for one correlated atom now.")

        # Full gap object
        gap = self.Re.Obj + 1j*self.Im.Obj
        # Gap in band basis
        gap_band = Gap(proj.nband, self.mesh, self.nfreq)
#        gap_band = np.zeros((self.mesh.nkpt, self.nfreq, proj.nband,
#                             proj.nband), dtype=complex)
        numk = 8*self.mesh.n[0]*self.mesh.n[1]*self.mesh.n[2]
        numdone = 0
        prct = 0
        # Calculating on each mesh point
        for nx, ny, nz in it.product(range(2*self.mesh.n[0]),
                                     range(2*self.mesh.n[1]),
                                     range(2*self.mesh.n[2])):
            if numdone / numk * 100 >= prct:
                print("[%d/100]" % prct)
                prct += 1
            numdone += 1
            # K point mesh index for the gap, should be a full mesh already
            i1_gap = self.mesh[nx, ny, nz]
            if i1_gap != -1:
                # Obtaining the k point mesh index on the projectors' mesh
                # This is a bit tricky but using the value in reciprocal basis,
                # it becomes easy
                m1 = self.mesh.fromNgetM([nx, ny, nz])
                n1_proj = pmesh.fromMgetN([m1.item(0), m1.item(1),
                                           m1.item(2)])
                n1x = round(n1_proj.item(0))
                n1y = round(n1_proj.item(1))
                n1z = round(n1_proj.item(2))
                # K point mesh index on the projectors' mesh
                i1_proj = pmesh[n1x, n1y, n1z]

                # Perhaps it is not defined since projectors mesh probably is
                # reduced. If defined, easy, otherwise calculated
                if i1_proj != -1:
                    pp = proj[i1_proj, 0]
                else:
                    pp = False
                    # Looking for a defined symmetric point
                    for sym in pmesh.sgroup.gs:
                        g = pmesh.sgroup.GtoMatrix(sym)
                        G = pmesh.sgroup.GtoOrb(sym)
                        n2 = g.dot(n1_proj-pmesh.N0)+pmesh.N0
                        n2 = pmesh.translate_in_FBZ([n2.item(0),
                                                     n2.item(1),
                                                     n2.item(2)])
                        n2x = round(n2[0])
                        n2y = round(n2[1])
                        n2z = round(n2[2])
                        i2 = pmesh[n2x, n2y, n2z]
                        if i2 != -1:
                            # Projectors' value
                            pp = G.dot(proj[i2, 0])
                            break
                    # No symmetric point found = error
                    if pp is False:
                        text = ("This n point has empty projectors and is "
                                "not link by symmetry to any other point "
                                "with none-empty projectors.")
                        raise ValueError(text)
                pp_dag = np.transpose(np.conj(pp))

                # Calculate the basis change for all frequencies
                for w in range(self.nfreq):
                    gap_band[i1_gap, w] = pp_dag.dot(gap[i1_gap, w]).dot(pp)

        # Return the gap function in the band basis
        return gap_band

    def __getitem__(self, key):
        return self.Re.Obj[key] + 1j*self.Im.Obj[key]

    def __setitem__(self, key, value):
        self.Re.Obj[key] = value.real
        self.Im.Obj[key] = value.imag
