import numpy as np


class Gamma:
    def __init__(self, ndim, U, J, Up=None, Jp=None):
        """Object for the vertex function. This represents the interactions
           between two particles or between a particle and a hole. Two incoming
           and two outcoming, so four basis index. In RPA, no dependence on
           k or w but maybe eventually.

        Parameters
        ----------
        ndim : int
               One-particle basis dimension.
        U : float
            Value for on-site interaction.
        J : float
            Value for Hund's coupling.
        Up : float
             Value for (???). Usually Up = U - 2J or Up = U - 5/2J.
        Jp : float
             Value for (???pair hoping or spin-flip???). Usually Jp = J.
        """
        print("Creating vertex function object (Gamma).")
        self.ndim = ndim
        self.U = U
        self.J = J

        if Up is None:
            self.Up = U - 2*J
        else:
            self.Up = Up
        if Jp is None:
            self.Jp = J
        else:
            self.Jp = Jp

        self.gammaD = np.zeros((ndim**2, ndim**2))
        self.gammaM = np.zeros((ndim**2, ndim**2))

    def constructRPA(self):
        """Constructing RPA vertex function.
        """
        print("Consructing RPA vertex function.")
        for L1 in range(self.ndim**2):
            l1 = int(L1 / self.ndim)
            l2 = L1 % self.ndim

            for L2 in range(self.ndim**2):
                l3 = int(L2 / self.ndim)
                l4 = L2 % self.ndim

                if l1 == l2 and l1 == l3 and l1 == l4:
                    self.gammaD[L1, L2] = self.U
                    self.gammaM[L1, L2] = self.U
                elif l1 == l3 and l2 == l4 and l1 != l2:
                    self.gammaD[L1, L2] = -self.Up + 2*self.J
                    self.gammaM[L1, L2] = self.Up
                elif l1 == l2 and l3 == l4 and l1 != l3:
                    self.gammaD[L1, L2] = 2*self.Up - self.J
                    self.gammaM[L1, L2] = self.J
                elif l1 == l4 and l2 == l3 and l1 != l2:
                    self.gammaD[L1, L2] = self.Jp
                    self.gammaM[L1, L2] = self.Jp
                else:
                    self.gammaD[L1, L2] = 0
                    self.gammaM[L1, L2] = 0

    def __getitem__(self, key):
        if not key:
            return self.gammaD
        elif key == 1:
            return self.gammaM

    def __setitem__(self, key, value):
        self.gammaD[key] = value[0]
        self.gammaM[key] = value[1]
