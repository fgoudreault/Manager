from .bases import PostProdBase
from .hamiltonian import Hk
import numpy as np


class SpectralF(PostProdBase):
    _loggername = "postprod.spectral_function"

    def __init__(self, nkpt, dim, nfreq, **kwargs):
        """Object that construct a spectral function from the hamiltonian
        and the self energy.

        Parameters
        ----------
        nkpt : int
               The number of kpts.
        dim : int
              The number of dimensions of the hamiltonian (number of bands).
        nfreq : int
                The number of frequencies of the self energy.
        """
        super().__init__(**kwargs)
        self.spectral_function = np.zeros((nfreq, nkpt, dim))
        self.dim = dim
        self.nkpt = nkpt
        self.nfreq = nfreq

        self.shape = '[ %d, %d, %d]' % (dim, nkpt, nfreq)
        self._logger.info("Spectral function well initiated and"
                          " has dimensions %s." % self.shape)

    def construct(self, hamiltonian, freqs=None, fermi=0,
                  self_energy=None, broad=0.01):
        """Construct the spectral function.

        Parameters
        ----------
        hamiltonian : Hk instance
                      An Hamiltonian object.
        freqs : list-like
                The list of frequencies (should be same units as the
                Hamiltonian.
        fermi : float, optional
                The fermi level, should be same units as the Hamiltonian.
        self_energy : array-like, optional
                      The self energy operator projected on the band basis.
                      Should be an array nkpt x nfreq x nband x nband.
                      If None, it is not added to the green function.
        broad : float, optional
                Imaginary broadening added to the denominator of the Green
                Function.
        """
        self._logger.info("Constructing the spectral function,"
                          " needing a Hamiltonian, a frequency grid, a fermi"
                          " energy and perhaps a SelfE.")

        if not isinstance(hamiltonian, Hk):
            raise TypeError("The hamiltonian is not an Hk instance.")

        # TODO: Should check nkpt, ndim, norb, nband
        # TODO: Also, SelfE only works in band basis here.
        # probably cannot vectorize anymore than that because of the matrix
        # inversion below.
        ident = np.identity(self.dim, dtype=complex)
        for k in range(self.nkpt):
            for ifreq, freq in enumerate(freqs):
                g_inv = (freq + fermi + broad*1j) * ident
                g_inv -= hamiltonian[k]
                if self_energy is not None:
                    g_inv -= self_energy[ifreq, k]
                g = np.linalg.inv(g_inv)
                self[ifreq, k] = (-1./np.pi *
                                  np.imag(np.diag(g)))

    def read(self, filename):
        """Read the spectral function from a file.

        Parameters
        ----------
        filename : str
                   The file path.
        """
        self._logger.info("Loading spetral function from file %s." % filename)

        with open(filename, 'r') as f:
            lines = f.readlines()

        for l, line in enumerate(lines):
            if not l:
                continue
            w = int(line.split()[0])
            k = int(line.split()[1])

            for i in range(self.dim):
                self[w, k, i] = float(line.split()[2 + i])
        self._logger.info("Loading done.")

    def write(self, filename):
        """Write the spectral function to a file.

        Parameters
        ----------
        filename : str
                   The file path.
        """
        self._logger.info("Writing out the Spectral function"
                          " in the file %s." % filename)

        with open(filename, 'w') as f:
            f.write("# freq     kpt\n")

            for w in range(self.nfreq):
                for k in range(self.nkpt):
                    Swk = "%d     %d" % (w, k)
                    for i in range(self.dim):
                        Swk += "     %.8f" % self[w, k, i]
                    f.write(Swk + "\n")
        self._logger.info("Writing done.")

    def _get_data(self):
        return self.spectral_function
