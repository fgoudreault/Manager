import itertools
import math

import numpy as np
import scipy.linalg as ln
from abisuite import HARTREE_TO_EV, AbinitDMFTEigFile

from .bases import PostProdBase
from .kptmesh import KptMesh
from .projectors import Projectors
from manager.PostProd import plotting
from .plotting import PlotParams


class Hk(PostProdBase):
    _loggername = "postprod.hamiltonian"

    def __init__(self, nkpt, dim, mesh=None, **kwargs):
        """Object that represents a Hamiltonian depending of the kpt.

        Parameters
        ----------
        nkpt : int
               The number of kpts.
        dim : int
              The number of dimensions.
        mesh : KptMesh
               Mesh on which the hamiltonian is defined.
        """
        super().__init__(**kwargs)
        self.hamiltonian = np.zeros((nkpt, dim, dim), dtype=complex)
        self.nkpt = nkpt
        self.dim = dim
        self.mesh = None
        if mesh:
            if not isinstance(mesh, KptMesh):
                raise TypeError("The mesh must be given as a KptMesh type!")
            self.mesh = mesh

    def read(self, _file):
        """Read the Hamiltonian from a file.

        Parameters
        ----------
        _file : str
                The file's path.
        """

        self._logger.info('Loading Hamiltonian from file ' + _file + '.')
        if _file.endswith(".Hk"):
            self._read_from_hkfile(_file)
        elif _file.endswith(".eig"):
            self._read_from_eigfile(_file)
        elif _file.endswith("_EIG"):
            self._read_from_EIGfile(_file)
        else:
            raise ValueError("Cannot read the file type from filename.")
        self._logger.info('Loading done.')

    def _read_from_hkfile(self, _file):
        with open(_file, 'r') as f:
            lines = f.readlines()
        dim = int(lines[0])
        nkpt = int(lines[1])

        # do some checkups
        assert dim == self.dim
        assert nkpt == len(self)

        for k in range(nkpt):
            i = 0
            hk = lines[k + 2].split()
            for n in range(dim):
                for m in range(dim):
                    ind = 2 * i
                    self[k, n, m] = (float(hk[ind]) +
                                     1j * float(hk[ind + 1]))
                    if n != m:
                        self[k, m, n] = (float(hk[ind]) +
                                         1j * float(hk[ind+1]))
                    i += 1

    def _read_from_eigfile(self, _file):
        with AbinitDMFTEigFile.from_file(_file) as eig_file:
            # check that everything matches
            if self.nkpt != eig_file.nkpt:
                raise ValueError("Number of kpts does not match.")
            if self.dim != eig_file.nband:
                raise ValueError("Number of bands does not match.")
            # for now we only do non spin polarized calculations. Thus,
            # we do not take into account the spins.
            data = eig_file.eigenvalues
            if len(np.shape(eig_file.eigenvalues)) > 2:
                # data is spin polarised, print warning and select first row.
                self._logger.warning(
                        "Data is spin polarized. Taking only first spin data.")
                data = data[0]
        # filling self and converting to eV
        data = np.array(data) * HARTREE_TO_EV
        for ikpt, kpt in enumerate(data):
            np.fill_diagonal(self[ikpt], kpt)

    def _read_from_EIGfile(self, _file):
        with open(_file, 'r') as f:
            lines = f.readlines()
        i = 0
        for line in lines[2:]:
            j = i % (math.ceil(self.dim/8) + 1)
            if j:
                k = int(i / (math.ceil(self.dim/8) + 1))
                for n, l in enumerate(line.split()):
                    self[k, 8*(j-1)+n, 8*(j-1)+n] = float(l) * HARTREE_TO_EV
            i += 1

    def write(self, hk_out='outfile.Hk', klist_out='outfile.klist'):
        """Writes the hamiltonian data into a file following a Kpt Mesh.

        Parameters
        ----------
        hk_out : str, optional
                 The hamiltonian file path.
        klist_out : str, optional
                    The kpt list file path that matches the hamiltonian.
        """
        if not isinstance(self.mesh, KptMesh):
            raise ValueError("This function is built"
                             " to work with a KptMesh only.")
        if self.mesh.nkpt > self.nkpt:
            raise ValueError("The mesh has more k pts"
                             " than the Hk object, which is impossible.")
        self._logger.info("Writing Hk.hamiltonian in the file " + hk_out +
                          " and the klist in the file " + klist_out + ".")
        # f_Hk = open(Hk_out, 'w')
        f_Hk = []
        f_Hk.append("%d\n" % self.dim)
        f_Hk.append("%d\n" % self.mesh.nkpt)

        # f_kl = open(klist_out, 'w')
        f_kl = []
        f_kl.append("%d\n" % self.mesh.nkpt)

        for nx, ny, nz in itertools.product(range(2*self.mesh.n[0]),
                                            range(2*self.mesh.n[1]),
                                            range(2*self.mesh.n[2])):
            if self.mesh[nx, ny, nz] != -1:

                m = self.mesh.fromNgetM([nx, ny, nz])
                f_kl.append("%.12f\t%.12f\t%.12f\n" %
                            (m.item(0), m.item(1), m.item(2)))

                # h0 = [[[0, 0] for n in range(self.dim)]
                #        for m in range(self.dim)]
                hk = ""
                for n in range(self.dim):
                    for m in range(self.dim):
                        o = self[self.mesh[nx, ny, nz]]
                        hk += ("%.10f\t%.10f\t" % (o[n, m].real, o[n, m].imag))
                hk += '\n'
                f_Hk.append(hk)
        with open(hk_out, 'w') as f:
            f.writelines(f_Hk)
        with open(klist_out, 'w') as f:
            f.writelines(f_kl)
        self._logger.info('Writing done.')

    def from_band_to_orb_basis(self, mesh=None, proj=None):
        # Make sure there is a mesh of type KptMesh and sets it as mesh.
        if not mesh or not isinstance(mesh, KptMesh):
            if not isinstance(self.mesh, KptMesh):
                raise TypeError("Mesh most be a Kpt Mesh.")
            mesh = self.mesh

        # Make sure there is a proj of type Projectors.
        if not proj:
            raise ValueError("The proj argument must be given to change "
                             "basis.")
        if not isinstance(proj, Projectors):
            raise TypeError("The proj argument must be of type Projectors.")

        # The change of basis requieres a well defined set of orbitals.
        if not len(mesh.sgroup.orbs):
            raise ValueError("Orbitals are not defined for the rotation of"
                             " the projectors! Define them using mesh.sgroup."
                             "SetOrbs().")

        # Temporary hamiltonian for each atoms
        new_eig = [Hk(mesh.nkpt, proj.norb) for at in range(proj.natom)]

        for k in range(mesh.nkpt):
            for at in range(proj.natom):
                new_eig[at][k] = (proj[k, at].dot(self[k]).dot(np.transpose(
                                  np.conj(proj[k, at]))))
        return new_eig

    def full_mesh_hamilt(self, mesh=None, proj=None, testmode=False,
                         write=None):
        """Returns the hamiltonian on a full 3D mesh. If projectors included,
           the hamiltonian will be basis changed.

        Parameters
        ----------
        mesh : kmesh.KptMesh
               The mesh in mandatory because the hamiltonian is returned as an
               array with the shape of the mesh, with the value of the
               hamiltonian on each k point site. If the mesh is not given here
               as an argument, it could have been given while initiializing the
               hamiltonian.
        proj : projectors.Projectors
               If it is desired that the hamiltonian is returned in the orbital
               basis, give the projectors as an argument, otherwise keep it
               None.
        testmode : bool
                   If you want to check the incertainty of your symmetrization,
                   give a hamiltonian (and projectors) on a full mesh and check
                   the consistency with regard to symmetrization.
        write : str array
                If you want to write down the resulting Hamiltonian instead of
                returning it, give the file for .Hk first and .klist second.
        """
        # Make sure there is a mesh of type KptMesh and sets it as mesh.
        if not mesh or not isinstance(mesh, KptMesh):
            if not isinstance(self.mesh, KptMesh):
                raise TypeError("Mesh most be a Kpt Mesh.")
            mesh = self.mesh

        # If proj is not None, take the orbital dimensions for the returned
        # hamiltonian and initiate projectors on a 3D tensor as the mesh.
        dim = self.dim
        mproj = None
        if proj and isinstance(proj, Projectors):
            dim = proj.norb
            mproj = np.zeros((2*mesh.n[0], 2*mesh.n[1], 2*mesh.n[2], proj.norb,
                              self.dim), dtype=complex)
            # The change of basis requieres a set of orbitals well defined
            if not len(mesh.sgroup.orbs):
                raise ValueError("Orbitals are not defined for the rotation of"
                                 " projectors! Define them using mesh.sgroup."
                                 "SetOrbs().")

        # Fill the band hamiltonian and possibly the mesh_projectors with given
        # information from the mesh
        for nx, ny, nz in itertools.product(range(2*mesh.n[0]),
                                            range(2*mesh.n[1]),
                                            range(2*mesh.n[2])):
            kpt = mesh.mesh[nx, ny, nz]
            if kpt != -1:
                if mproj is not None:
                    mproj[nx, ny, nz] = proj.projectors[kpt]

        # nk = mesh.nkpt
        # nosym = 0
        # pre_mesh = 0

        # Take all symmetry matrices from the sgroup
        gs = mesh.sgroup.gs

        # There is a phase factor that can differ in projectors
        if testmode:
            Ns = []
            for i in range(2**self.dim):
                N = np.diag(np.ones(self.dim))
                for j in range(2**self.dim):
                    if not j:
                        if i % 2:
                            N[j][j] = -1
                    else:
                        if (i % 2**(j+1)) > 2**j-1:
                            N[j][j] = -1
                Ns.append(N)

        # Use the symmetries of the sgroup to fill the band_hamilt and mproj
        # Variables for %
        numk = 2*mesh.n[0]*2*mesh.n[1]*2*mesh.n[2]
        numdone = 0
        prct = 0
        # Loop on all points of the mesh
        for nx, ny, nz in itertools.product(range(2*mesh.n[0]),
                                            range(2*mesh.n[1]),
                                            range(2*mesh.n[2])):
            if numdone / numk * 100 >= prct:
                print("[%d/100]" % prct)
                prct += 1
            numdone += 1
            # If the point is in the FBZ and ((none set and not in testmode)
            # or (set and in testmode))
            if (mesh.in_FBZ(mesh.fromNgetM([nx, ny, nz]))
                and ((not testmode and mesh[nx, ny, nz] == -1) or
                     (testmode and mesh[nx, ny, nz] != -1))):
                # Setting stats
                # filled = 0
                bad_energy = 0
                bad_proj = 0

                # Write the point in tensor index
                n = np.transpose(np.matrix((nx, ny, nz)))
                # Convert in (kx,ky,kz) basis
                k = ln.inv(mesh.S).dot(n-mesh.N0)

                # Loop on the symmetry matrices
                for g in gs:
                    # Apply symmetry
                    k2 = mesh.sgroup.GtoMatrix(g).dot(k)
                    # Rewrite in tensor index
                    n2 = mesh.S.dot(k2)+mesh.N0
                    n2 = mesh.translate_in_FBZ([n2.item(0), n2.item(1),
                                                n2.item(2)])
                    n2x = round(n2[0])
                    n2y = round(n2[1])
                    n2z = round(n2[2])

                    # If n2, symmetry-related to n1, is none empty
                    if mesh[n2x, n2y, n2z] != -1:
                        G = mesh.sgroup.GtoOrb(g)
                        # No testmode = assign
                        if not testmode:
                            mesh[nx, ny, nz] = mesh[n2x, n2y, n2z]
                            if mproj is not None:
                                new_proj = G.dot(mproj[n2x, n2y, n2z])
                                mproj[nx, ny, nz] = new_proj
                            # nk += 1
                            # filled = 1
                            break
                        if mproj is None:
                            break

                        prec = 5

                        # Check energies
                        ham1 = self[mesh[nx, ny, nz]]
                        ham2 = self[mesh[n2x, n2y, n2z]]
                        if (ham1.round(prec) != ham2.round(prec)).any():
                            bad_energy += 1
                            self._logger.debug("Band energy:")
                            self._logger.debug(ham1.round(prec))
                            self._logger.debug(ham2.round(prec))

                        # Check mesh projectors
                        m1 = mproj[nx, ny, nz].round(prec)
                        m2 = mproj[n2x, n2y, n2z].round(prec)
                        condition = (m1 != G.dot(m2)).any()
                        if condition:
                            prob = 1
                            # Check phase in band basis
                            for N in Ns:
                                condition = (G.dot(m2).dot(N) == m1).all()
                                if condition:
                                    prob = 0
                                    break
                            if prob:
                                bad_proj += 1
                                self._logger.debug("Sym = %s" % g)
                                self._logger.debug("Bad projectors:")
                                self._logger.debug(m1)
                                self._logger.debug(G.dot(m2))
        # Exit testmode
        if testmode:
            if not bad_energy and not bad_proj:
                return True
            else:
                raise ValueError("Problems with symmetrization in testmode.")

        # Now we should have everything to compute the band or
        # orbital hamiltonian on the full mesh
        full_nkpt = 0
        if mproj is None:
            final_hamilt = np.zeros((2*mesh.n[0], 2*mesh.n[0], 2*mesh.n[2],
                                     dim, dim), dtype=complex)

            for nx, ny, nz in itertools.product(range(2*mesh.n[0]),
                                                range(2*mesh.n[1]),
                                                range(2*mesh.n[2])):
                k = mesh[nx, ny, nz]
                if k != -1:
                    full_nkpt += 1
                    final_hamilt[nx, ny, nz] = self[k]
        else:
            final_hamilt = np.zeros((2*mesh.n[0], 2*mesh.n[0], 2*mesh.n[2],
                                     dim, dim), dtype=complex)

            for nx, ny, nz in itertools.product(range(2*mesh.n[0]),
                                                range(2*mesh.n[1]),
                                                range(2*mesh.n[2])):
                k = mesh[nx, ny, nz]
                if k != -1:
                    full_nkpt += 1
                    kproj = mproj[nx, ny, nz]
                    kproj2 = np.transpose(np.conj(kproj))
                    Ho = kproj.dot(self[k]).dot(kproj2)
                    final_hamilt[nx, ny, nz] = Ho

        if write is not None and len(write) == 2:
            f_Hk = []
            f_Hk.append("%d\n" % dim)
            f_Hk.append("%d\n" % full_nkpt)

            # f_kl = open(klist_out, 'w')
            f_kl = []
            f_kl.append("%d\n" % full_nkpt)

            for nx, ny, nz in itertools.product(range(2*mesh.n[0]),
                                                range(2*mesh.n[1]),
                                                range(2*mesh.n[2])):
                if mesh[nx, ny, nz] != -1:
                    m = mesh.fromNgetM([nx, ny, nz])
                    f_kl.append("%.12f\t%.12f\t%.12f\n" %
                                (m.item(0), m.item(1), m.item(2)))

                    # h0 = [[[0, 0] for n in range(self.dim)]
                    #        for m in range(self.dim)]
                    hk = ""
                    for n in range(dim):
                        for m in range(dim):
                            o = final_hamilt[nx, ny, nz]
                            hk += ("%.10f\t%.10f\t" % (o[n, m].real,
                                                       o[n, m].imag))
                    hk += '\n'
                    f_Hk.append(hk)
            with open(write[0], 'w') as f:
                f.writelines(f_Hk)
            with open(write[1], 'w') as f:
                f.writelines(f_kl)
            self._logger.info('Writing done.')

        return final_hamilt

    def plotInPlane(self, fermi, cmap=None, cutoff=0., broad=0.05,
                    params=None, fill=False, selfE=None):
        if params is None:
            params = PlotParams()
        if not isinstance(params, PlotParams):
            raise TypeError("Error: params must be of type PlotParams or"
                            " None.")

        if selfE is None:
            selfE = np.zeros((self.dim, self.dim))
        else:
            if len(selfE) != self.dim:
                raise ValueError("Error: selfE must be of the same dimensions "
                                 "the hamiltonian.")
            selfE = np.diag(selfE)

        m = self.mesh
        if m is None:
            raise ValueError("Error: self.mesh needs to be setted.")

        mu = (fermi + 1j*broad)*np.identity(self.dim)

        G = np.zeros((m.nkpt, self.dim, self.dim), dtype=complex)
        A = np.zeros((self.dim, 2*m.n[0], 2*m.n[1]))
        for k, kpt in enumerate(m.npts):
            G[k] = ln.inv(mu - self[k] - selfE)
            for d in range(self.dim):
                if kpt[2] == 0:
                    A[d][kpt[0]][kpt[1]] = -1./math.pi*np.imag(G[k][d][d])

        if fill:
            k_set = itertools.product(range(2*m.n[0]), range(2*m.n[1]))
            for kx, ky in k_set:
                kz = m.N0.item(2)
                if m[kx, ky, kz] == -1:
                    k2 = m.translate_in_FBZ([kx, ky, kz])
                    for d in range(self.dim):
                        A[d][kx][ky] = A[d][round(k2[0])][round(k2[1])]

        default = PlotParams()
        default["figsize"] = (10, 10)
        default["subplots"] = [1, 1]

        params.setDefaults(default)

        fig, ax = plotting.setFigure(params)

        if isinstance(cutoff, float) or isinstance(cutoff, int):
            cutoff = [cutoff for d in range(self.dim)]
        elif len(cutoff) != self.dim:
            raise ValueError("Error: dimension of cutoff is incorrect.")

        for d in range(self.dim):
            mask = np.ma.masked_where(A[d] < cutoff[d], A[d])
            if cmap is not None:
                ax.imshow(mask, cmap=cmap[d], origin="lower")
            else:
                ax.imshow(mask, origin="lower")

        if params["special1"]:
            plotting.plotSpecial1(ax, m.n, params)
        if params["special2"]:
            plotting.plotSpecial2(ax, m.n, params)

        return fig, ax

    def __len__(self):
        return len(self.hamiltonian)

    def _get_data(self):
        return self.hamiltonian
