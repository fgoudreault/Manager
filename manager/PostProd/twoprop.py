import numpy as np
from .physobj import PhysObj
import itertools as it


class TwoPropagator():
    def __init__(self, ndim, nfreq, nkpt):
        """Object description a two-body propagator. It implies two incoming
           objects and two outcoming ones. So in a given one-body basis {l},
           for either particle-particle (pp) or particle-hole (ph), it is an
           object such that
           [G^{pp/ph}(k, w)]_{l_1l_2;l_3l_4}

        Parameters
        ----------
        ndim : int
               Dimension of a one-body propagators, of the basis.
        nfreq : int
                Number of frequencies of the object.
        nkpt : int
               Number of k points.
        """
        self.Re = PhysObj(ndim**2, nkpt, nfreq)
        self.Im = PhysObj(ndim**2, nkpt, nfreq)

        self.ndim = ndim
        self.nfreq = nfreq
        self.nkpt = nkpt

    def leadingInstabilities(self, gamma, nlead=1):
        r"""Returns the nlead leading k point where the instability is more
           diverging. When dressing the bare propagators with a vertex
           function, divergences occur when \pm \Gamma\G_0^{ph} approaches
           unity, + for magnetic channel and - for density channel.

        Parameters
        ----------
        gamma : Gamma
                This object is a matrix with the same dimensions as self. It
                encodes the two-particle interactions.
        nlead : int
                Number of leading k points returned.
        """
        print(gamma[1])
        print(self[0, 0])
        eig = np.zeros((2, self.nkpt, self.nfreq), dtype=complex)
        for k in range(self.nkpt):
            for w in range(self.nfreq):
                den = -1*gamma[0].dot(self[k, w])
                mag = gamma[1].dot(self[k, w])
                """
                print(gamma[0])
                print(den)
                print(gamma[1])
                print(mag)
                """
                eig[0, k, w] = np.amax(np.linalg.eigvals(den))
                eig[1, k, w] = np.amax(np.linalg.eigvals(mag))
                # print("k : %d; w: %d; Density: %f+j%f; Magnetic: %f+j%f" %
                #       (k, w, eig[0, k, w].real, eig[0, k, w].imag,
                #       eig[1, k, w].real, eig[1, k, w].imag))

        index_den = eig[0, :, 0].argsort(axis=0)
        index_mag = eig[1, :, 0].argsort(axis=0)

        print(den)

        print(eig[0, :, 0].real)
        print(index_den)

        print("Density:")
        k_den = []
        for i in range(1, nlead+1):
            k = index_den[-i]
            k_den.append(k)
            print("rank: %d; k index: %d; value: %f+i%f" %
                  (i, k, eig[0, k, 0].real, eig[0, k, 0].imag))

        print("Magnetic:")
        k_mag = []
        for i in range(1, nlead+1):
            k = index_mag[-i]
            k_mag.append(k)
            k = index_mag[-i]
            print("rank: %d; k index: %d; value: %f+i%f" %
                  (i, k, eig[1, k, 0].real, eig[1, k, 0].imag))

        return eig, [k_den, k_mag]

    def dimSubMatrix(self, sub_index, write=None):
        """Returns a sub matrix for each k and w.

        Parameters
        ----------
        sub_index : int list
                    One-particle states to keep. The TwoPropagator as 4 indices
                    here only given the incices such as [1, 2] for ndim = 3.
        write : str
                Filename of output.
        """
        sub = TwoPropagator(len(sub_index), self.nfreq, self.nkpt)

        ll = []
        for l1, l2 in it.product(sub_index, sub_index):
            ll.append(l1*self.ndim+l2)

        sub.Re.Obj = self.Re[np.ix_(range(self.nkpt), range(self.nfreq),
                             ll, ll)]
        sub.Im.Obj = self.Im[np.ix_(range(self.nkpt), range(self.nfreq),
                             ll, ll)]

        return sub

    def __getitem__(self, key):
        return self.Re[key] + 1j*self.Im[key]

    def __setitem__(self, key, value):
        self.Re[key] = value.real
        self.Im[key] = value.imag

    def __len__(self):
        return len(self.Re.Obj)
