from abisuite import AbinitDMFTProjectorsFile
import numpy as np
import scipy.linalg as ln

from .bases import PostProdBase


class Projectors(PostProdBase):
    _loggername = "postprod.projectors"

    def __init__(self, norb, nband, nkpt, natom=1, **kwargs):
        """Object that represents the projectors onto the orbital basis.

        Parameters
        ----------
        norb : int
               The number of orbitals.
        nband : int
                The number of bands.
        nkpt : int
               The number of k-points.
        natom : int
                The number of correlated atoms.
        """
        super().__init__(**kwargs)
        self.projectors = np.zeros((nkpt, natom, norb, nband), dtype=complex)

        self.natom = natom
        self.norb = norb
        self.nband = nband
        self.nkpt = nkpt

        self._logger.info("Projectors well initiated and"
                          " has dimensions (nkpt, natom, norb, nband) = %s."
                          % str(self.projectors.shape))

    def read(self, _file):
        """Reads the projectors from a file.

        Parameters
        ----------
        _file : str
                The path to the projectors file.
        """
        self._logger.info("Loading the projectors from the file %s." %
                          _file)
        with AbinitDMFTProjectorsFile.from_file(_file) as proj_file:
            # need to swap band and orb axes
            data = np.swapaxes(proj_file.projectors, 1, 4)
        data = np.swapaxes(data, 1, 3)
        # for now assume 1 spin and 1 atom only
        self[:, :, :, :] = data[:, :, 0, :, :]
        self._logger.info("Projectors loaded!")

    def orthonormalize(self):
        """Orthonormalizes the projectors.
        """
        self._logger.info("Orthonormalization of the projectors.")

        nproj = Projectors(self.norb, self.nband, self.nkpt, self.natom)
        # TODO: improve the vectorization of this calculation
        for k in range(self.nkpt):
            for at in range(self.natom):
                orth_proj = np.zeros((self.norb, self.norb), dtype=complex)
                projk = self[k, at]

                for n in range(self.norb):
                    for m in range(self.norb):
                        prod = projk[n].dot(np.conj(projk[m]))
                        orth_proj[n, m] = prod

                orth_proj_sqrt = ln.sqrtm(ln.inv(orth_proj))

                for n in range(self.norb):
                    for a in range(self.nband):
                        toadd = orth_proj_sqrt[n, :].dot(projk[:, a].T)
                        nproj[k, at, n, a] += toadd

        self.projectors = nproj.projectors
        self._logger.info("Orthonormalization done!\n")

    def check_orthonormalization(self, klist='first'):
        """Checks that the orthonormalization is correct.

        Parameters
        ----------
        klist : str, optional, {'first', 'all'}
                If set to 'first' the checkup is done only on the first kpt.
                If set to 'all', the checkup is done on all kpts.
        """
        if klist == "first":
            klist = [0]
        elif klist == "all":
            klist = range(self.nkpt)
        else:
            raise ValueError("klist argument should be 'first' or 'all'.")
        results = []
        alert = 0
        # TODO: improve the vectorization of this calculation
        for k in klist:
            for at in range(self.natom):
                _id = np.zeros((self.norb, self.norb), dtype=complex)
                projk = self[k, at]
                for m in range(self.norb):
                    for n in range(self.norb):
                        _id[n, m] += projk[n].dot(np.conj(projk[m]))
                np.set_printoptions(linewidth=132, precision=5)
                if not alert:
                    if np.max(np.around(_id, 8) - np.identity(self.norb)) > 0:
                        alert = 1
                results.append(np.around(_id, 8))
        if alert:
            message = "NOT GOOD."
        else:
            message = "Well done."

        self._logger.info("Checked orthonormalization: %s" % message)
        return results

    def _get_data(self):
        return self.projectors
