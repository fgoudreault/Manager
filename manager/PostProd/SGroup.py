import pylab as pl
import numpy as np
import numpy.linalg as ln
import re


class SGroup:
    def __init__(self, _sgroup, _orbs=''):
        self.sgroup = _sgroup

        if self.sgroup == 139:
            self.R1 = pl.transpose(pl.matrix(((1/2., 1/2., -1/2.),
                                              (-1/2., 1/2., 1/2.),
                                              (1/2., -1/2., 1/2.))))
            self.R2 = ln.inv(self.R1)
            self.B = pl.transpose(self.R2)

            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'Sp+I', 'Sm+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'Sp+R', 'Sm+R',
                       'C1+I', 'C2+I', 'C3+I', 'C1+R', 'C2+R', 'C3+R']

        elif self.sgroup == 71:
            self.R1 = pl.transpose(pl.matrix(((1/2., 1/2., -1/2.),
                                              (-1/2., 1/2., 1/2.),
                                              (1/2., -1/2., 1/2.))))
            self.R2 = ln.inv(self.R1)
            self.B = pl.transpose(self.R2)

            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'C2+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'C2+R']

        elif self.sgroup == "cube":
            self.R1 = np.diag((1, 1, 1))
            self.R2 = self.R1
            self.B = self.R2

            self.gs = ['I+I+I', 'R+I+I', 'I+R+I', 'Sp+I', 'Sm+I',
                       'I+I+R', 'R+I+R', 'I+R+R', 'Sp+R', 'Sm+R',
                       'C1+I', 'C2+I', 'C3+I', 'C1+R', 'C2+R', 'C3+R']

        else:
            raise ValueError("This SGroup value is not available.")

        self.orbs = []
        if not _orbs == '':
            if not len(_orbs):
                print("ERROR in SGroup.__init__: Orbs should be passed as an "
                      "array of strings. Left undefined. Use SGroup.SetOrbs.")
            else:
                self.SetOrbs(_orbs)

    def SetOrbs(self, orbs):
        for o in orbs:
            if o not in ['xy', 'yz', 'zx']:
                raise ValueError("ERROR in SGroup.SetOrbs: The orbital %s does"
                                 " not exist in this module. Please change it,"
                                 " contact us or contribute to the project.")
            self.orbs.append(o)

    def GtoMatrix(self, g):
        if not isinstance(g, str):
            raise TypeError("g has to be a string describing the symmetry.")

        reg = re.compile(r'\w+')
        match = reg.findall(g)

        M = pl.zeros((3, 3))

        i = 0
        for m in match:

            if m == 'I':
                M[i, i] = 1
                i += 1

            elif m == 'R':
                M[i, i] = -1
                i += 1

            elif m == 'Sp':
                M[i, i + 1] = 1
                M[i + 1, i] = 1
                i += 2

            elif m == 'Sm':
                M[i, i + 1] = -1
                M[i + 1, i] = -1
                i += 2

            elif m == 'C1':
                M[i, i + 1] = 1
                M[i + 1, i] = -1
                i += 2

            elif m == 'C2':
                M[i, i] = -1
                M[i + 1, i + 1] = -1
                i += 2

            elif m == 'C3':
                M[i, i + 1] = -1
                M[i + 1, i] = 1
                i += 2
        return M

    def GtoOrb(self, g):
        if not isinstance(g, str):
            raise TypeError("g has to be a string describing the symmetry.")

        norb = len(self.orbs)

        M = self.GtoMatrix(g)
        MO = pl.zeros((norb, norb))

        Olist = []
        for orb in self.orbs:
            o = pl.zeros((3, 1))
            if orb == 'xy':
                o[0, 0] = 1
                o[1, 0] = 1
            elif orb == 'yz':
                o[1, 0] = 1
                o[2, 0] = 1
            elif orb == 'zx':
                o[0, 0] = 1
                o[2, 0] = 1
            else:
                raise ValueError("This orb is not available. "
                                 "Make sure it is well written.")
            Olist.append(o)

        for i, o in enumerate(Olist):
            if self.orbs[i] in ['xy', 'yz', 'zx']:
                o1 = M.dot(o)

                norm = 1
                for j in o1:
                    if j:
                        norm *= j
                o1 = norm*pl.absolute(o1)

                for j, o2 in enumerate(Olist):
                    if (o2 == o1).all():
                        MO[i, j] = 1
                    elif (o2 == -1*o1).all():
                        MO[i, j] = -1

        return MO
