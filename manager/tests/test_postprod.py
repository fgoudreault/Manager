from manager.PostProd import KptMesh
from manager.PostProd import Projectors
from manager.PostProd import Hk
from manager.PostProd import Gap
from manager.PostProd import TwoPropagator
from manager.PostProd import Gamma
import unittest
import os
import numpy as np
import filecmp
# from manager.PostProd import SelfE
# from manager.PostProd import SpectralF


Ha_to_eV = 27.2113845
dirpath = os.path.dirname(__file__)


class TestKptMest(unittest.TestCase):
    def test_kptmesh_01(self):
        """From an abinit output with all k points (kptopt = 1) in a
           conventional 10x10x20 BZ (kptrlatt = [[10, 10, 0], [10, 0, 10],
           [0, 10, 10]]), maps all the points in the first BZ.
        """
        filepath = "%s/test_kptmesh_01" % dirpath
        nkpt = 2000

        mesh = KptMesh.read("%s/Input/calc.out" % filepath, [10, 10, 10], 139,
                            nkpt=nkpt)
        ref = np.load("%s/Ref/mesh.npy" % filepath)

        self.assertTrue(np.testing.assert_allclose(mesh.mesh, ref) is None,
                        'Output mesh is different than the reference mesh.')

    def test_kptmesh_02(self):
        """From an abinit output with only reduced k points (kptopt = 3) in a
           conventional 10x10x20 BZ (kptrlatt = [[10, 10, 0], [10, 0, 10],
           [0, 10, 10]]), check the symmetrization of the mesh,
        """
        filepath = "%s/test_kptmesh_02" % dirpath
        nkpt = 201

        KptMesh.read("%s/Input/calc.out" % filepath, [10, 10, 10], 139,
                     nkpt=nkpt)
        # Add symmetries function and apply here
        # TODO: Compare with a ref

    def test_kptmesh_03(self):
        """From an abinit full mesh in conventional 10x10x20 BZ, take a
           commensurate submesh and incommensurate submesh.
        """
        filepath = "%s/test_kptmesh_03" % dirpath
        nkpt = 2000

        mesh = KptMesh.read("%s/Input/calc.out" % filepath, [10, 10, 10], 139,
                            nkpt=nkpt)
        comm_sub = mesh.getSubmesh([5, 5, 5])
        ref = np.load("%s/Ref/comm_submesh.npy" % filepath)

        self.assertTrue(np.testing.assert_allclose(comm_sub.mesh, ref) is None,
                        "Output submesh is different than the ref mesh.")

        try:
            mesh.getSubmesh([3, 3, 3])
        except ValueError:
            pass

    def test_kptmesh_04(self):
        """Load a kpt mesh from a type file.out and print it as a
           file.klist."""
        filepath = "%s/test_kptmesh_04" % dirpath
        nkpt = 2000
        mesh = KptMesh.read("%s/Input/calc.out" % filepath, [10, 10, 10], 139,
                            nkpt=nkpt)
        tempfile = "%s/mesh_write.klist" % filepath
        mesh.write("klist", tempfile)
        ref = "%s/Ref/system.klist" % filepath

        self.assertTrue(filecmp.cmp(tempfile, ref),
                        "Written klist file is different than the ref.")
        os.remove(tempfile)

    def test_kptmesh_05(self):
        """Test the different types in read function."""
        filepath = "%s/test_kptmesh_05" % dirpath

        # for _type in ["out", "klist", "kmesh"]:
        for _type in ["out", "klist", "kmesh"]:
            try:
                KptMesh.read("%s/Input/system.%s" % (filepath, _type),
                             [10, 10, 10], 139, nkpt=2000)
            except NotImplementedError:
                pass


class TestProjectors(unittest.TestCase):
    def test_projectors_01(self):
        """This test check many things at once: initializes a Projectors obj,
           read it from a test file, orthonormalizes it and than checks that
           the orthonormalization procedure work using the function that
           calculates the product of projectors.
        """
        filepath = "%s/test_projectors_01" % dirpath
        norb = 3
        nband = 6
        nkpt = 201

        proj = Projectors(norb, nband, nkpt)
        proj.read("%s/Input/forlb.ovlp" % filepath)
        proj.orthonormalize()
        results = proj.check_orthonormalization("all")
        for r in results:
            identity = np.diag([1]*norb)
            if not (r == identity).all():
                self.fail("Projectors not orthonormalizable.")


class TestHamiltonian(unittest.TestCase):
    def test_hamiltonian_01(self):
        """Construct a hamiltonian from a read .eig file, on a mesh constructed
           from a calc.out file, and writes it as a .Hk file with corresponding
           .klist file.
        """
        filepath = "%s/test_hamiltonian_01" % dirpath
        nband = 6
        nkpt = 2000

        mesh = KptMesh.read("%s/Input/calc.out" % filepath, [10, 10, 10], 139,
                            nkpt=nkpt)
        eig = Hk(nkpt, nband, mesh)
        eig.read("%s/Input/forlb.eig" % filepath)

        tempfile_hk = "%s/system.Hk" % filepath
        tempfile_klist = "%s/system.klist" % filepath

        eig.write(hk_out=tempfile_hk, klist_out=tempfile_klist)

        ref_hk = "%s/Ref/system.Hk" % filepath
        ref_klist = "%s/Ref/system.klist" % filepath

        self.assertTrue(filecmp.cmp(tempfile_hk, ref_hk),
                        "Written Hk file is different than the ref.")
        self.assertTrue(filecmp.cmp(tempfile_klist, ref_klist),
                        "Written klist file is different than the ref.")
        os.remove(tempfile_hk)
        os.remove(tempfile_klist)

    def test_hamiltonian_02(self):
        """Reads hamiltonian from a .Hk file.
        """
        filepath = "%s/test_hamiltonian_02" % dirpath
        nkpt = 2000
        nband = 6

        eig = Hk(nkpt, nband)
        eig.read("%s/Input/system.Hk" % filepath)

    def test_hamiltonian_03(self):
        """Test None file.
        """
        eig = Hk(0, 0)
        try:
            eig.read("nofile")
        except ValueError:
            pass

    def test_hamiltonian_04(self):
        """There is a calculation on the full mesh and one on the reduced BZ
           only. We compare the band_hamiltonian on the full mesh with the one
           obtained by using symmetry operations. The output, for well
           converged energy values, essentially be the same.
        """
        filepath = "%s/test_hamiltonian_04" % dirpath
        nband = 6

        # Read the full mesh and correponding band hamiltonian
        nkpt_full = 2000
        mesh_full = KptMesh.read("%s/Input/calc_full.out" % filepath,
                                 [10, 10, 10], 139, nkpt=nkpt_full)
        eig_full = Hk(nkpt_full, nband, mesh_full)
        eig_full.read("%s/Input/forlb_full.eig" % filepath)

        # Read the reduced mesh and corresponding band hamiltonian
        nkpt_red = 201
        mesh_red = KptMesh.read("%s/Input/calc_red.out" % filepath,
                                [10, 10, 10], 139, nkpt=nkpt_red)
        eig_red = Hk(nkpt_red, nband, mesh_red)
        eig_red.read("%s/Input/forlb_red.eig" % filepath)

        # Calculate the full mesh hamiltonian and check equity
        condition = (eig_red.full_mesh_hamilt() ==
                     eig_full.full_mesh_hamilt()).all()
        self.assertTrue((condition, "Symmetrization of hamiltonian is not "
                         "working properly."))

    def test_hamiltonian_05(self):
        """There is a calculation on the full mesh and one on the reduced BZ
           only. We compare the orb_hamiltonian on the full mesh with the one
           obtained by using symmetry perations. We need the projectors for
           this one. A good converence of the projectors is more heavy.
        """
        filepath = "%s/test_hamiltonian_05" % dirpath
        norb = 3
        nband = 6

        # Load the full mesh, associate the ensemble of orbitals, load the
        # projectors, load the corresponding hamiltonian
        nkpt_full = 2000
        mesh_full = KptMesh.read("%s/Input/calc_full.out" % filepath,
                                 [10, 10, 10], 139, nkpt=nkpt_full)
        mesh_full.sgroup.SetOrbs(["xy", "yz", "zx"])
        proj_full = Projectors(norb, nband, nkpt_full)
        proj_full.read("%s/Input/forlb_full.ovlp" % filepath)
        proj_full.orthonormalize()
        eig_full = Hk(nkpt_full, nband, mesh_full)
        eig_full.read("%s/Input/forlb_full.eig" % filepath)

        # Same thing on the reduced mesh
        nkpt_red = 201
        mesh_red = KptMesh.read("%s/Input/calc_red.out" % filepath,
                                [10, 10, 10], 139, nkpt=nkpt_red)
        mesh_red.sgroup.SetOrbs(["xy", "yz", "zx"])
        proj_red = Projectors(norb, nband, nkpt_red)
        proj_red.read("%s/Input/forlb_red.ovlp" % filepath)
        proj_red.orthonormalize()
        eig_red = Hk(nkpt_red, nband, mesh_red)
        eig_red.read("%s/Input/forlb_red.eig" % filepath)

        # Calculate the full mesh hamiltonian and check error
        condition = np.amax(eig_full.full_mesh_hamilt(proj=proj_full) -
                            eig_red.full_mesh_hamilt(proj=proj_red)) < 1e-8
        self.assertTrue((condition, "Symmetrization of hamiltonian is not "
                         "working properly."))


class TestGap(unittest.TestCase):
    def test_gap_01(self):
        """Test the breaking symmetries of a given gap function. In this case,
           the gap is d-wave and breaks 4 symmetries. Only spatial symmetries
           so far, will include frequency as well.
        """
        filepath = "%s/test_gap_01" % dirpath
        norb = 3
        nfreq = 4

        mesh = KptMesh.read("%s/Input/system.klist" % filepath, [4, 4, 2], 139)
        mesh.sgroup.SetOrbs(["xy", "yz", "zx"])

        gap = Gap(norb, mesh, nfreq)
        gap.Re.Read("%s/Input/system.ReGapFn" % filepath, _skip=1)
        gap.Im.Read("%s/Input/system.ImGapFn" % filepath, _skip=1)

        broken_sym = gap.TestAllSym(precision=1e-6)
        ref = ["odd-w"]
        self.assertTrue(set(broken_sym) == set(ref), "Broken symmetries are "
                                                     "different than ref.")

    def test_gap_02(self):
        """
        """
        filepath = "%s/test_gap_02" % dirpath
        norb = 3
        nband = 6
        nfreq = 4

        mesh_proj = KptMesh.read("%s/Input/calc.out" % filepath, [12, 12, 6],
                                 139)
        mesh_proj.sgroup.SetOrbs(["xy", "yz", "zx"])

        proj = Projectors(norb, nband, mesh_proj.nkpt)
        proj.read("%s/Input/projectors.ovlp" % filepath)

        mesh_gap = KptMesh.read("%s/Input/system.qlist" % filepath,
                                [4, 4, 2], 139, _type="klist")
        mesh_gap.sgroup.SetOrbs(["xy", "yz", "zx"])

        gap = Gap(norb, mesh_gap, nfreq)
        gap.Re.Read("%s/Input/system.ReGapFn" % filepath, _skip=1)
        gap.Im.Read("%s/Input/system.ImGapFn" % filepath, _skip=1)

        gap.changeBasis(proj, mesh_proj)

    def test_gap_03(self):
        filepath = "%s/test_gap_03" % dirpath
        norb = 3
        nfreq = 4

        mesh1 = KptMesh.read("%s/Input/system.klist" % filepath,
                             [4, 4, 2], 139)

        gap = Gap(norb, mesh1, nfreq)
        gap.Re.Read("%s/Input/system.ReGapFn" % filepath, _skip=1)
        gap.Im.Read("%s/Input/system.ImGapFn" % filepath, _skip=1)

        mesh2 = KptMesh([6, 6, 4], 139)

        gap2 = gap.interpolate(mesh2)

        ref_Re = np.load("%s/Ref/ReGapObj.npy" % filepath)
        ref_Im = np.load("%s/Ref/ImGapObj.npy" % filepath)

        ass = (np.testing.assert_allclose(gap2.Re.Obj, ref_Re) is None,
               'Real part of the gap is different than the reference.')
        self.assertTrue(ass)

        ass = (np.testing.assert_allclose(gap2.Im.Obj, ref_Im) is None,
               'Imag part of the gap is different than the reference.')
        self.assertTrue(ass)


class TestTwoPropagator(unittest.TestCase):
    def test_twoprop_01(self):
        """Load a bare two-particle propagator from .Re/.ImBareChipp. Calculate
           the Stoner facter and return nine leading instabilities with
           assiciated k points.
        """
        filepath = "%s/test_twoprop_01" % dirpath
        norb = 3
        nv = 5
        U = 0.9
        J = 0.3

        mesh = KptMesh.read("%s/Input/system.klist" % filepath, [4, 4, 2], 139)

        barechi = TwoPropagator(norb, nv, mesh.nkpt)
        barechi.Re.Read("%s/Input/system.ReBareSusph" % filepath, _skip=2)
        barechi.Im.Read("%s/Input/system.ImBareSusph" % filepath, _skip=2)

        gamma = Gamma(norb, U, J)
        gamma.constructRPA()

        eig, ks = barechi.leadingInstabilities(gamma, nlead=4)
        ref = [44, 57, 61, 41]
        for r in ref:
            if r not in ks[1]:
                raise AssertionError()

    def test_twoprop_02(self):
        """
        """
        filepath = "%s/test_twoprop_02" % dirpath
        norb = 3
        nfreq = []
        orbs = [1, 2]

        mesh = KptMesh.read("%s/Input/system.klist" % filepath, [4, 4, 2], 139)

        filelist = ["BareSusph", "BareSuspp", "BareChipp"]
        nfreq = [5, 1, 3]
        topline = []
        topline.append("# Bosonic Freq.,  q index")
        topline.append("# Fermionic Freq., k index")
        topline.append("# Fermionic Freq., k index")

        for i, f in enumerate(filelist):
            barechi = TwoPropagator(norb, nfreq[i], mesh.nkpt)
            barechi.Re.Read("%s/Input/system.Re%s" % (filepath, f), _skip=2)
            barechi.Im.Read("%s/Input/system.Im%s" % (filepath, f), _skip=2)

            sub = barechi.dimSubMatrix(orbs)

            tline_Re = (topline[i] + "\n# Re%s(index1, index2) where index1, "
                        "index2 are composite orbital indices") % f
            tline_Im = (topline[i] + "\n# Im%s(index1, index2) where index1, "
                        "index2 are composite orbital indices") % f
            tempfile_Re = "%s/system.Re%s" % (filepath, f)
            tempfile_Im = "%s/system.Im%s" % (filepath, f)
            sub.Re.write(tempfile_Re, tline_Re)
            sub.Im.write(tempfile_Im, tline_Im)

            ref = "%s/Ref/system.Re%s" % (filepath, f)
            self.assertTrue(filecmp.cmp(tempfile_Re, ref),
                            "The file Re%s is different than the ref." % f)
            os.remove(tempfile_Re)
            ref = "%s/Ref/system.Im%s" % (filepath, f)
            self.assertTrue(filecmp.cmp(tempfile_Im, ref),
                            "The file Im%s is different than the ref." % f)
            os.remove(tempfile_Im)


class TestBandStruct(unittest.TestCase):
    def test_bandstruct_01(self):
        filepath = "%s/test_bandstruct_01" % dirpath
        norb = 3
        nband = 6
        # fermi = 0.274224 * Ha_to_eV
        # broad = 0.005
        # nfreq = 50
        # useself = False

        nkpt = 31

        mesh = KptMesh.read("%s/Input/calc.out" % filepath, [20, 20, 1], 139,
                            nkpt=nkpt)
        # kpts = [mesh.mesh[29, 10+x, 0] for x in range(20)]

        proj = Projectors(norb, nband, nkpt)
        proj.read("%s/Input/forlb.ovlp" % filepath)

        eig = Hk(nkpt, nband, mesh)
        eig.read("%s/Input/forlb.eig" % filepath)

        # self = SelfE(norb, nfreq)

# spec = SpectralF(nkpt, nband, nfreq)
# spec.Construct(eig, Ha_to_eV * self.freqs, fermi, None, _broad = broad)
