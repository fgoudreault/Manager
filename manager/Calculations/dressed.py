from .calculation import Calculation
from .barechi2 import Barechi2
from manager.PostProd import PhysObj, KptMesh, Gamma, PlotParams
from colorama import Fore, Back, Style

from matplotlib.ticker import FormatStrFormatter
import pylab as pl
import numpy as np
import matplotlib.pyplot as plt
import colorama
import re
import os
import subprocess
import itertools


class Dressed(Calculation):

    def __init__(self, system, id=-1):
        Calculation.__init__(self, system)
        self.table = "dressed"
        self.prevTable = ["barechi2"]
#        self.prevTable = ["barechi2", "barechi1", "dmft"]
        self.prevCalc = Barechi2(system)

        if id != -1:
            self.id = id
            self.Load(True)

        self.params = {"dressedU": None, "dressedJ": None,
                       "miwn": None, "mivn": None}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)
        with open("%s/Dressed/Input.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        with open("%s/Dressed/IntParam.dat" % filePath, "r") as f:
            fileIntParam = f.read()

        def diffUJ():
            return str(float(self.params["dressedU"]) -
                       2*float(self.params["dressedJ"]))
        self.funcParams = {"diffUJ": diffUJ}

        self.treeDir = "U-$(dressedU)/J-$(dressedJ)/"

        self.tree = {"Input/": {"Input.dat": fileInput,
                                "%s.intparam" % system: fileIntParam,
                                "[%s.klist]" % system: ("&(barechi2.barechi2"
                                                        "dir)Input/%s.qlist" %
                                                        system),
                                "[%s.ReBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ReBareSusph" %
                                                              system),
                                "[%s.ImBareSusph]" % system: ("&(barechi2."
                                                              "barechi2dir)"
                                                              "Output/%s."
                                                              "ImBareSusph" %
                                                              system)
                                },
                     "Output/": {},
                     }

    def SetEliash(self):
        if self.id == -1:
            print("%sError: You cannot plot from this calculation because"
                  " it is not defined in the database. Please load it"
                  " using self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.cells = self.Load(all=True)

        from manager.Calculations import Eliash
        eliash = Eliash(self.system)
        eliash.prevId = self.id
        eliash.prevCalc = self

        return eliash

    def Launch(self):
        colorama.init()

        os.chdir(self.conn.groundDir + self.cells["dressed.dresseddir"] +
                 "Input/")
        print("%sExecuting BareVertex.py%s" % (Back.YELLOW + Fore.BLACK,
                                               Style.RESET_ALL))
        subprocess.call("python3 /mnt/parallel_scratch_mp2_wipe"
                        "_on_december_2017/"
                        "tremblay/gingras1/Programs/DressedChi/"
                        "BareVertex.py %s" % self.system, shell=True)

        os.chdir("..")
        print("%sExecuting DressedChi%s" % (Back.YELLOW + Fore.BLACK,
                                            Style.RESET_ALL))
        print("\t%sSaving output file in Dressed.out%s" % (Fore.YELLOW,
                                                           Fore.RESET))
        subprocess.call("/mnt/parallel_scratch_mp2_wipe_on_december_2017/"
                        "tremblay/gingras1/Programs/DressedChi/DressedChi"
                        "> Dressed.out", shell=True)

        print("\t%sFinding both Stoner factor from the file%s" % (Fore.YELLOW,
                                                                  Fore.RESET))
        with open("Dressed.out", "r") as f:
            first = True
            StonerD = 0
            StonerM = 0
            for line in f:
                if re.search("Stoner factor:", line):
                    if first:
                        StonerD = float(line.split()[4])
                        first = False
                    else:
                        StonerM = float(line.split()[4])
        print("\t%sStonerD is %f;\t StonerM is %f%s" % (Fore.MAGENTA, StonerD,
                                                        StonerM, Fore.RESET))
        self.conn.Query("execute", "UPDATE dressed SET stonerD = %f, stonerM ="
                        "%f WHERE dressedid = %d" % (StonerD, StonerM,
                                                     self.id))

    def ReturnBareSusc(self, nkpt):
        print("Dressed.ReturnBareSusc: Returns a PhysObj with the BareChi ph."
              " THIS SHOULD BE DONE IN BARECHI2.")

        norb = self.cells["barechi2.norb"]
        nfreq = 10

        Obj = PhysObj(norb**2, nkpt, nfreq)

        Obj.Read(self.conn.groundDir + self.baseDir +
                 'Input/Sr2RuO4.ReBareSusph', 2)

        return Obj.Obj

    def PlotDressedSus(self, nkpts, sgroup, toPlot="all", plotParams=None):
        if plotParams is None:
            plotParams = {}
        colorama.init()

        if self.id == -1:
            print("%sError: You cannot plot from this calcuylation because it"
                  " is not defined in the database. Please load it using"
                  " self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.cells = self.Load(all=True)

        Mesh = KptMesh(nkpts, _sgroup=sgroup)

        Mesh.read("%s%sU-%s/J-%s/Input/%s.klist" %
                  (self.conn.groundDir, self.baseDir,
                   str(self.cells["dressed.dressedU"]),
                   str(self.cells["dressed.dressedJ"]), self.system))

        if self.cells['barechi2.norb'] != 3:
            print("%sError: norb is %d, case not worked out yet.%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return False

        if toPlot == "all":
            toPlot = {}
            toPlot["ext"] = ["Density", "Magnetic"]
            toPlot["mivn"] = range(self.cells["dressed.mivn"]+1)

        import pylab as pl
        import mpl_toolkits.axes_grid1.inset_locator as inset_axes

        nnorb = self.cells["barechi2.norb"]**2

        ll = [[0, 0], [4, 4], [8, 8]]
        llabels = [r"\alpha", r"\beta", r"\gamma"]

        for ext in toPlot["ext"]:
            ReObj = PhysObj(nnorb, Mesh.nkpt, self.cells["dressed.mivn"] + 1)
            ReObj.Read("%s%sU-%s/J-%s/Output/%s.ReDressedSusph%s" %
                       (self.conn.groundDir, self.baseDir,
                        str(self.cells["dressed.dressedU"]),
                        str(self.cells["dressed.dressedJ"]),
                        self.system, ext), _skip=2)

            ImObj = PhysObj(nnorb, Mesh.nkpt, self.cells["dressed.mivn"] + 1)
            ImObj.Read("%s%sU-%s/J-%s/Output/%s.ImDressedSusph%s" %
                       (self.conn.groundDir, self.baseDir,
                        str(self.cells["dressed.dressedU"]),
                        str(self.cells["dressed.dressedJ"]),
                        self.system, ext), _skip=2)

            if ext == "Density":
                tag = "d"
            elif ext == "Magnetic":
                tag = "m"

            for m in toPlot["mivn"]:
                print("")
                fig, ax = pl.subplots(1, self.cells["barechi2.norb"],
                                      figsize=(8, 4))
                for l in range(len(ll)):

                    Real = ReObj.ReturnInPlane(Mesh, [ll[l], m])
                    print("Real: \t Min: %f \t Max: %f" % (pl.amin(Real),
                                                           pl.amax(Real)))

                    Reimg = ax[l].imshow(Real, origin="lower",
                                         cmap=pl.get_cmap("bwr"))

                    ax[l].set_title(r"$\Re\{\chi^%s_{ph}(\vec{q}, i\omega_"
                                    r"{ %s })_{ %s, %s} \}$" %
                                    (tag, m, llabels[l], llabels[l]))
                    ax[l].set_xticks([], [])
                    ax[l].set_yticks([], [])
                    ax[l].set_xlim([0, nkpts[0] - 1])
                    ax[l].set_ylim([0, nkpts[1] - 1])

                    Reinset_ax = inset_axes.inset_axes(ax[l], width="3%",
                                                       height="85%", loc=2)
                    Recbar = pl.colorbar(Reimg, cax=Reinset_ax,
                                         format="%01.01e")
                    Recbar.ax.tick_params(labelsize=10)

                    Recbar.set_ticks([pl.amin(Real), pl.amax(Real)])

                    r'''
                    Imag = ImObj.ReturnInPlane(Mesh, [ll[l], m])
                    print("Imag: \t Min: %f \t Max: %f" %
                          (pl.amin(Imag), pl.amax(Imag)))

                    Imimg = ax[1, l].imshow(Imag, origin="lower",
                                            cmap = pl.get_cmap("bwr"))

                    ax[1, l].set_title(r"$\Im\{\chi^%s_{ph}(\vec{q},"
                                       r"i\omega_{ %s })_{ %s, %s} \}$" %
                                       (tag, m, llabels[l], llabels[l]))
                    ax[1, l].set_xticks([], [])
                    ax[1, l].set_yticks([], [])
                    ax[1, l].set_xlim([0, nkpts[0] - 1])
                    ax[1, l].set_ylim([0, nkpts[1] - 1])

                    Iminset_ax = inset_axes.inset_axes(ax[1, l], width = "3%",
                                                       height = "85%", loc = 2)
                    Imcbar = pl.colorbar(Imimg, cax=Iminset_ax,
                                         format="%01.01e")
                    Imcbar.ax.tick_params(labelsize=10)

                    Imcbar.set_ticks([pl.amin(Imag),pl.amax(Imag)])
                    '''

                savename = ("%s%sU-%s/J-%s/%s_w-%d.png" %
                            (self.conn.groundDir, self.baseDir,
                             str(self.cells["dressed.dressedU"]),
                             str(self.cells["dressed.dressedJ"]), ext, m))

                print("Save figure: %s" % savename)
                pl.savefig(savename)
#                pl.show()
                pl.draw()

    def PlotDressedSusPath(self, plotParams=None):
        if plotParams is None:
            plotParams = {}
        # Initialize the color code for printing
        colorama.init()

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""
        if len(self.cells) == 0:
            self.cells = self.Load()

        # Find parameters constructing the object
        ndim = self.cells["barechi2.norb"]
        nqpath = self.cells["barechi2.nqpath"]
        nfreqs = self.cells["barechi2.nivn"]

        default = PlotParams()
        default.params["indices"] = [[[0, 0], [4, 4], [8, 8]],
                                     [[1, 1], [2, 2], [5, 5]]]
        default.params["plot_colors"] = [["blue", "red", "green"],
                                         ["black", "orange", "purple"]]
        default.params["labels"] = ["xy", "yz", "zx"]
        default.params["xticks"] = [[0, (nqpath - 1) / 3 - 1,
                                     2 * (nqpath - 1) / 3 - 1, nqpath - 1],
                                    [r"$\Gamma$", "M", "X", r"$\Gamma$"]]
        default.params["title"] = ("Dressed Susceptibilities in particle-hole "
                                   "(ph) and particle-particle (pp) channels\n"
                                   "U = %f; J = %f" %
                                   (self.cells["dressed.dressedU"],
                                    self.cells["dressed.dressedJ"]))
        default.params["freqs"] = range(nfreqs)
        plotParams.setDefaults(default)

        extensions = ["ReDressedSusphDensity", "ReDressedSusphMagnetic"]

        for v in plotParams["freqs"]:
            fig, ax = pl.subplots(2, len(extensions),
                                  figsize=plotParams["figsize"])
            for e, extension in enumerate(extensions):
                # Make filename
                filename = ("%sOutput/%s.%s" %
                            (self.cells["dressed.dresseddir"],
                             self.system, extension))
                print("%sYou have requested to plot the file %s%s%s" %
                      (Fore.MAGENTA, Style.BRIGHT, filename, Style.RESET_ALL))

                # Construct physical object
                obj = PhysObj(ndim**2, nqpath,
                              self.cells["dressed.mivn"] + 1)
                obj.Read(self.conn.groundDir + filename, _skip=2)

                diag = [[obj.Trace[q][v][l] for q in range(nqpath)]
                        for l in range(ndim**2)]

                # Make the ylabel for the figure
                match = re.match(r"^(\w{2})DressedSusph(\w+)$", extension)
                if match:
                    if match.group(2) == "Density":
                        channel = 'd'
                    elif match.group(2) == "Magnetic":
                        channel = "m"

                    ylabel = (r"$[\chi^%s_{ph}(i\nu_%d,"
                              r"\vec{q})]_{l_1l_2}$" % (channel, v))

                else:
                    ylabel = ""

                for j, ll, in enumerate(plotParams["indices"]):
                    for i, l in enumerate(ll):
                        print("Print %i" % i)
                        if not l[0] == l[1]:
                            raise ValueError("Susceptibilities are diagonal.")

                        label1 = plotParams["labels"][int(l[0] / ndim)]
                        label2 = plotParams["labels"][int(l[0] % ndim)]
                        ax[e, j].plot(pl.arange(nqpath), diag[l[0]],
                                      color=plotParams["plot_colors"][j][i],
                                      label="%s;%s" %
                                      (label1, label2),
                                      marker='', linestyle='-', linewidth=3)
                    ax[e, j].set_xlim([0, nqpath-1])

                    pl.sca(ax[e, j])
                    pl.xticks(plotParams["xticks"][0], plotParams["xticks"][1])

                    if plotParams["yticks"] is not None:
                        pl.yticks(plotParams["yticks"][e][0],
                                  plotParams["yticks"][e][1])

                    ymin, ymax = pl.ylim()
                    for t in range(1, len(plotParams["xticks"][0]) - 1):
                        ax[e, j].plot([plotParams["xticks"][0][t],
                                       plotParams["xticks"][0][t]],
                                      [ymin, ymax],
                                      color="black", linestyle='--')
                    format = FormatStrFormatter("%.1f")
                    ax[e, j].yaxis.set_major_formatter(format)

                    if j == 0:
                        ax[e, j].set_ylabel(ylabel, fontsize=16)

                    ax[e, j].legend(loc="best")

            pl.suptitle(plotParams["title"])
            pl.savefig(self.conn.groundDir + self.baseDir +
                       "DressedSus_v%d.png" % v)

            if plotParams["tight_layout"] is True:
                pl.tight_layout()

            if plotParams["show"]:
                pl.show()
            else:
                pl.draw()

    def PlotLadderPath(self, plotParams=None):
        if plotParams is None:
            plotParams = {}
        # Initialize the color code for printing
        colorama.init()

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""

        nkpt = self.cells["barechi2.nqpath"]
        nqpath = self.cells["barechi2.nqpath"]

        if "indices" in plotParams:
            _indices = plotParams["indices"]
        else:
            raise ValueError("ERROR: Too many possibilities for indices, "
                             "please specify in plotParams[\"indices\"].")

        if "plot_colors" in plotParams:
            _plot_colors = plotParams["plot_colors"]
        else:
            _plot_colors = ([["blue", "red", "green"],
                            ["black", "orange", "purple"]])
        assert(len(_indices) == len(_plot_colors))

        if "figsize" in plotParams:
            _figsize = plotParams["figsize"]
        else:
            _figsize = (6, 5)

        if "titlesize" in plotParams:
            _titlesize = plotParams["titlesize"]
        else:
            _titlesize = 10

        if "labels" in plotParams:
            _labels = plotParams["labels"]
        else:
            _labels = ["z", "x", "y"]

        if "xticks" in plotParams:
            _xticks = plotParams["xticks"]
        else:
            _xticks = ([[0, nqpath / 3 - 1, nqpath * 2 / 3 - 1, nqpath - 1],
                       [r"$\Gamma$", "M", "X", r"$\Gamma$"]])

        if "freqs" in plotParams:
            _freqs = plotParams["freqs"]
        else:
            _freqs = range(self.cells["dressed.mivn"] + 1)

        if "complexParts" in plotParams:
            _complexParts = plotParams["complexParts"]
        else:
            _complexParts = ["Re", "Im"]

        if "show" in plotParams:
            _show = plotParams["show"]
        else:
            _show = False

        dU = self.cells["dressed.dressedU"]
        dJ = self.cells["dressed.dressedJ"]
        ndim = self.cells["barechi2.norb"]
        G = Gamma(ndim, dU, dJ)
        G.constructRPA()

        title = ("Ladder functions in density (d) and magnetic (m) channels\n"
                 "U = %f; J = %f" % (dU, dJ))

        for c in _complexParts:
            if c not in ["Re", "Im"]:
                raise ValueError("%s in plotParams[\"complexParts\"] "
                                 "is not recognised." % c)
            Den = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Den.Read("%sOutput/%s.%sDressedSusphDensity" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)
            Mag = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Mag.Read("%sOutput/%s.%sDressedSusphMagnetic" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)

            print(G[0] * Den[0][0] * G[0])

            for v in _freqs:
                fig, ax = plt.subplots(2, len(_indices), figsize=_figsize)
                Ladder_d = np.zeros((nkpt, ndim**2, ndim**2))
                Ladder_m = np.zeros((nkpt, ndim**2, ndim**2))

                ylabel = ([r"$[\Phi_{%s}(i\nu_{%d}, \vec{q})]_"
                          "{l_1,l_2;l_3,l_4}$" % (t, v)
                           for t in ["d", "m"]])

                for k in range(nkpt):
                    Ladder_d[k] = G[0].dot(Den[k][v].dot(G[0]))
                    Ladder_m[k] = G[1].dot(Mag[k][v].dot(G[1]))

                for j, ll in enumerate(_indices):
                    for i, l in enumerate(ll):
                        ax[0, j].plot(np.arange(nqpath),
                                      Ladder_d[:, l[0], l[1]],
                                      label="%s%s;%s%s" %
                                      (_labels[int(l[0] / ndim)],
                                      _labels[int(l[0] % ndim)],
                                      _labels[int(l[1] / ndim)],
                                      _labels[int(l[1] % ndim)]),
                                      marker='', linestyle='-', linewidth=3,
                                      color=_plot_colors[j][i])
                        ax[1, j].plot(np.arange(nqpath),
                                      Ladder_m[:, l[0], l[1]],
                                      label="%s%s;%s%s" %
                                      (_labels[int(l[0] / ndim)],
                                      _labels[int(l[0] % ndim)],
                                      _labels[int(l[1] / ndim)],
                                      _labels[int(l[1] % ndim)]),
                                      marker='', linestyle='-', linewidth=3,
                                      color=_plot_colors[j][i])

                    ax[0, j].set_xlim([0, nqpath-1])
                    ax[1, j].set_xlim([0, nqpath-1])

                    plt.sca(ax[0, j])
                    plt.sca(ax[1, j])

                    ax[0, j].set_xticks(_xticks[0], _xticks[1])
                    ax[0, j].set_xticks(_xticks[0], _xticks[1])

                    ymin0, ymax0 = ax[0, j].get_ylim()
                    ymin1, ymax1 = ax[1, j].get_ylim()
                    for t in range(1, len(_xticks[0]) - 1):
                        ax[0, j].plot([_xticks[0][t], _xticks[0][t]],
                                      [ymin0, ymax0],
                                      color="black", linestyle='--')
                        ax[1, j].plot([_xticks[0][t], _xticks[0][t]],
                                      [ymin1, ymax1],
                                      color="black", linestyle='--')
                    format = FormatStrFormatter("%.1f")
                    ax[0, j].yaxis.set_major_formatter(format)
                    ax[1, j].yaxis.set_major_formatter(format)

                    if j == 0:
                        ax[0, j].set_ylabel(ylabel[0], fontsize=16)
                        ax[1, j].set_ylabel(ylabel[1], fontsize=16)

                    ax[0, j].legend(loc="best")
                    ax[1, j].legend(loc="best")

                plt.suptitle(title, fontsize=_titlesize)
                plt.savefig(self.conn.groundDir + self.baseDir +
                            "%sLadders_v%d.png" % (c, v))
                if _show:
                    plt.show()
                else:
                    plt.draw()

    def PlotVertexPath(self, plotParams=None):
        if plotParams is None:
            plotParams = {}
        # Initialize the color code for printing
        colorama.init()

        # Valid calculation needed
        if self.id == -1:
            print("%sError: You cannot plot from the calculation because it"
                  " is not defined in the database. Please load it using "
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""

        nkpt = self.cells["barechi2.nqpath"]
        nqpath = self.cells["barechi2.nqpath"]

        if "indices" in plotParams:
            _indices = plotParams["indices"]
        else:
            raise ValueError("ERROR: Too many possibilities for indices, "
                             "please specify in plotParams[\"indices\"].")

        if "plot_colors" in plotParams:
            _plot_colors = plotParams["plot_colors"]
        else:
            _plot_colors = ([["blue", "red", "green"],
                            ["black", "orange", "purple"]])
        assert(len(_indices) == len(_plot_colors))

        if "figsize" in plotParams:
            _figsize = plotParams["figsize"]
        else:
            _figsize = (6, 5)

        if "titlesize" in plotParams:
            _titlesize = plotParams["titlesize"]
        else:
            _titlesize = 10

        if "labels" in plotParams:
            _labels = plotParams["labels"]
        else:
            _labels = ["z", "x", "y"]

        if "xticks" in plotParams:
            _xticks = plotParams["xticks"]
        else:
            _xticks = [[0, nqpath / 3 - 1, nqpath * 2 / 3 - 1, nqpath - 1],
                       [r"$\Gamma$", "M", "X", r"$\Gamma$"]]

        if "freqs" in plotParams:
            _freqs = plotParams["freqs"]
        else:
            _freqs = range(self.cells["dressed.mivn"] + 1)

        if "complexParts" in plotParams:
            _complexParts = plotParams["complexParts"]
        else:
            _complexParts = ["Re", "Im"]

        if "show" in plotParams:
            _show = plotParams["show"]
        else:
            _show = False

        dU = self.cells["dressed.dressedU"]
        dJ = self.cells["dressed.dressedJ"]
        ndim = self.cells["barechi2.norb"]
        G = Gamma(ndim, dU, dJ)
        G.constructRPA()

        for c in _complexParts:
            if c not in ["Re", "Im"]:
                raise ValueError("%s in plotParams[\"complexParts\"] "
                                 "is not recognised." % c)
            Den = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Den.Read("%sOutput/%s.%sDressedSusphDensity" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)
            Mag = PhysObj(ndim**2, nkpt, self.cells["dressed.miwn"])
            Mag.Read("%sOutput/%s.%sDressedSusphMagnetic" %
                     (self.conn.groundDir + self.baseDir,
                      self.system, c), _skip=2)

            print(G[0] * Den[0][0] * G[0])

            for v in _freqs:
                for chan in ["s", "t"]:
                    if chan == "s":
                        channel = "singlet"
                    elif chan == "t":
                        channel = "triplet"
                    title = ("Pairing vertex components in %s channel\n"
                             "U = %f; J = %f" % (channel, dU, dJ))

                    fig, ax = plt.subplots(2, len(_indices), figsize=_figsize)
                    Ladder_d = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_m = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_plus = np.zeros((nkpt, ndim**2, ndim**2))
                    Ladder_minus = np.zeros((nkpt, ndim**2, ndim**2))

                    ylabel = ([r"$[\Phi^{%s}(i\nu_{%d}, \vec{q})]_"
                              "{l_1,l_2;l_3,l_4}$" % (sign, v)
                               for sign in ["-", "+"]])

                    for k in range(nkpt):
                        Ladder_d[k] = G[0].dot(Den[k][v].dot(G[0]))
                        Ladder_m[k] = G[1].dot(Mag[k][v].dot(G[1]))

                        ind = itertools.product(range(ndim), range(ndim),
                                                range(ndim), range(ndim))
                        for l1, l2, l3, l4 in ind:
                            ll1 = l1*ndim+l2
                            ll2 = l3*ndim+l4

                            ll3 = l2*ndim+l4
                            ll4 = l3*ndim+l1
                            ll5 = l1*ndim+l4
                            ll6 = l3*ndim+l2

                            a1 = Ladder_d[k][ll3, ll4]
                            a2 = Ladder_m[k][ll3, ll4]

                            b1 = Ladder_d[k][ll5, ll6]
                            b2 = Ladder_m[k][ll5, ll6]

                            if chan == "s":
                                Ladder_minus[k][ll1, ll2] = .5*a1 - 1.5*a2
                                Ladder_plus[k][ll1, ll2] = .5*b1 - 1.5*b2

                            if chan == "t":
                                Ladder_minus[k][ll1, ll2] = .5*a1 + .5*a2
                                Ladder_plus[k][ll1, ll2] = -.5*b1 - .5*b2

                    for j, ll in enumerate(_indices):
                        for i, l in enumerate(ll):
                            ax[0, j].plot(np.arange(nqpath),
                                          Ladder_minus[:, l[0], l[1]],
                                          label="%s%s;%s%s" %
                                          (_labels[int(l[0] / ndim)],
                                          _labels[int(l[0] % ndim)],
                                          _labels[int(l[1] / ndim)],
                                          _labels[int(l[1] % ndim)]),
                                          marker='', linestyle='-',
                                          linewidth=3,
                                          color=_plot_colors[j][i])
                            ax[1, j].plot(np.arange(nqpath),
                                          Ladder_plus[:, l[0], l[1]],
                                          label="%s%s;%s%s" %
                                          (_labels[int(l[0] / ndim)],
                                          _labels[int(l[0] % ndim)],
                                          _labels[int(l[1] / ndim)],
                                          _labels[int(l[1] % ndim)]),
                                          marker='', linestyle='-',
                                          linewidth=3,
                                          color=_plot_colors[j][i])

                        ax[0, j].set_xlim([0, nqpath-1])
                        ax[1, j].set_xlim([0, nqpath-1])

                        plt.sca(ax[0, j])
                        plt.sca(ax[1, j])

                        ax[0, j].set_xticks(_xticks[0], _xticks[1])
                        ax[0, j].set_xticks(_xticks[0], _xticks[1])

                        ymin0, ymax0 = ax[0, j].get_ylim()
                        ymin1, ymax1 = ax[1, j].get_ylim()
                        for t in range(1, len(_xticks[0]) - 1):
                            ax[0, j].plot([_xticks[0][t], _xticks[0][t]],
                                          [ymin0, ymax0],
                                          color="black", linestyle='--')
                            ax[1, j].plot([_xticks[0][t], _xticks[0][t]],
                                          [ymin1, ymax1],
                                          color="black", linestyle='--')
                        format = FormatStrFormatter("%.1f")
                        ax[0, j].yaxis.set_major_formatter(format)
                        ax[1, j].yaxis.set_major_formatter(format)

                        if j == 0:
                            ax[0, j].set_ylabel(ylabel[0], fontsize=16)
                            ax[1, j].set_ylabel(ylabel[1], fontsize=16)

                        ax[0, j].legend(loc="best")
                        ax[1, j].legend(loc="best")

                    plt.suptitle(title, fontsize=_titlesize)
                    plt.savefig(self.conn.groundDir + self.baseDir +
                                "%sPairing_chan%s_v%d.png" % (c, chan, v))
                    if _show:
                        plt.show()
                    else:
                        plt.draw()
