from Calculations.Calculation import Calculation


class AbinitCalculation(Calculation):

    def __init__(self, system, prevCalc=False):
        Calculation.__init__(self, system)

        self.tree = {"calc.in": None,
                     "input_data/": {},
                     "run/": {"calc.files": None,
                              "tmp_data/": {},
                              "out_data/": {}
                              }
                     }

        self.abinitParams = {}
