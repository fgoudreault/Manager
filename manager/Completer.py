import os
import re


class Completer(object):
    def __init__(self, groundDir):
        self.groundDir = groundDir
        self.options = []
        self.setOptions("")

    def setOptions(self, text):
        match = re.match(r"^(.*/)?([-_\.\+\w]*)$", text)
        if match.group(1) is None:
            path = self.groundDir
        else:
            path = "%s/%s" % (self.groundDir, match.group(1))
        self.options = [p + "/" for p in sorted(next(os.walk(path))[1])]

    def complete(self, text, state):
        self.setOptions(text)
        match = re.match(r"^(.*/)?([-_\.\+\w]*)$", text)

        if match.group(1) is None:
            subtext = ""
        else:
            subtext = match.group(1)
#            print("subtext %s" % subtext)

        if state == 0:
            if text:
                #  print([subtext + o for o in self.options])
                self.matches = [s for s in [subtext + o for o in self.options]
                                if s and s.startswith(text)]
            else:
                self.matches = [subtext + o for o in self.options[:]]

        try:
            return self.matches[state]
        except IndexError:
            return None
