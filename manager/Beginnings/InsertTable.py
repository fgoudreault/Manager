

def InsertTable(_conn, _table):
    bar = "MySQL.InsertTable"
    if _table == "main":
        query = "CREATE TABLE main  ( "\
                "table_name VARCHAR(255) NOT NULL, "\
                "home_dir VARCHAR(255) NOT NULL, "\
                "server_dir VARCHAR(255) NOT NULL, "\
                "mainid VARCHAR(255), "\
                "directlink VARCHAR(255) "\
                ")"
        _conn.Query("execute", query)

    else:
        query = ("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE"
                 " TABLE_SCHEMA='manager' AND TABLE_NAME='" + _table + "'")
        if _conn.Query("select", query)[0][0]:
            print(bar + "ERROR in " + bar +
                  ": Table already exists, drop it first.")
        else:
            query = ("SELECT COUNT(*) FROM main WHERE table_name='" +
                     _table + "'")
            if _conn.Query("select", query)[0][0]:
                query = "DELETE FROM main WHERE table_name='" + _table + "'"
                _conn.Query("execute", query)
            query = ""

            if _table == "dmft":
                query = ("CREATE TABLE dmft ( "
                         "dmftid INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "dmftdir VARCHAR(255) NOT NULL, "
                         "dmftU FLOAT(3), "
                         "dmftJ FLOAT(3), "
                         "family VARCHAR(1), "
                         "norb INT(6) NOT NULL, "
                         "temp FLOAT(6) NOT NULL "
                         ")")

            elif _table == "strain":
                query = ("CREATE TABLE strain ( "
                         "strainid INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "dmftid INT(6) UNSIGNED NOT NULL, FOREIGN KEY(dmftid)"
                         " REFERENCES dmft(dmftid), "
                         "straindir VARCHAR(255) NOT NULL, "
                         "effm VARCHAR(255), "
                         "fill VARCHAR(255) "
                         ")")

            elif _table == "barechi1":
                query = ("CREATE TABLE barechi1 ( "
                         "barechi1id INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "strainid INT(6) UNSIGNED NOT NULL, FOREIGN"
                         " KEY(strainid) REFERENCES strain(strainid), "
                         "kgrid VARCHAR(255) NOT NULL "
                         ")")

            elif _table == "barechi2":
                query = ("CREATE TABLE barechi2 ( "
                         "barechi2id INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "barechi1id INT(6) UNSIGNED NOT NULL, FOREIGN"
                         " KEY(barechi1id) REFERENCES barechi1(barechi1id), "
                         "qgrid VARCHAR(255) NOT NULL, "
                         "chempot FLOAT(8), "
                         "niwn INT(6), "
                         "nivn INT(6) "
                         ")")

            elif _table == "dressed":
                query = ("CREATE TABLE dressed ( "
                         "dressedid INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "barechi2id INT(6) UNSIGNED NOT NULL, FOREIGN"
                         " KEY(barechi2id) REFERENCES barechi2(barechi2id), "
                         "dressedU FLOAT(5) NOT NULL, "
                         "dressedJ FLOAT(5), "
                         "miwn INT(6), "
                         "mivn INT(6) "
                         ")")

            elif _table == "eliash":
                query = ("CREATE TABLE eliash ( "
                         "eliashid INT(6) UNSIGNED NOT NULL AUTO_INCREMENT"
                         " PRIMARY KEY, "
                         "dressedid INT(6) UNSIGNED NOT NULL, FOREIGN"
                         " KEY(dressedid) REFERENCES dressed(dressedid), "
                         "parity BOOLEAN, "
                         "liwn INT(6), "
                         "livn INT(6) "
                         ")")

            if query != "":
                _conn.Query("execute", query)
            else:
                print("ERROR in " + bar + ": Table not set.")
