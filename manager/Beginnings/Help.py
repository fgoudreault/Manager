from PrintSelect import PrintSelect


# This function serves as an interface between someone and the database
# It helps out to print, update, insert or delete rows from the database.
def Help(_conn, _table="", _join=None, _where=None):

    if _join is None:
        _join = []

    if _where is None:
        _where = []

    # ######### Defining useful functions for Do() ##########
    # Prints the available options
    def PrintOptions():
        print(bar + ".PrintOptions:")
        print("\tNeed information?")
        print("\t\t'-t' : Print the table list.")
        print("\t\t'-d $t1 ... $tn' : Print the n table descriptions."
              " If no variables, prints the current joined tables'"
              " descriptions.")

        print("\tChange with tables?")
        print("\t\t'-j $t $v1 $v2' : Join to $t table via"
              " variables $v1 and $v2.")
        print("\t\t'-c $t' : Change main table, current on is " + table +
              ".")

        print("\tChange the constraints?")
        print("\t\t'-w $w[1,1] $w[1,2] ... $w[n,1] $w[n,2]' :"
              " Takes n conditions and prints all of them.")

        print("\tActions to perform?")
        print("\t\t'-s $v1 ... $vn' : Select a list of n variable"
              " from the current joined table.")
        print("\t\t'-i $v1 ... $vn' : Insert the n requiered variables."
              " To know these variables, just type '-i'.")
        print("\t\t'-u $v[1,1] $v[1,2] ... $v[n,1] $v[n,2]' : Update"
              " variables $v[:,1] with values $v[:,2]. Don't forget"
              " the where constrains!")
        print("\t\t'-r' : Remove (delete) rows. Don't forget"
              " the where constrains!")
        print()

        print("\tQuery so far is '" + action + " FROM " + table +
              JoinText(join) + WhereText(where) + "'.")
        if action == "?action":
            print("\tQuery cannot be performed yet because"
                  " of missing action.")
        else:
            print("\tWrite 'DO' to perform the query.")
        print()

    # Makes one choose a table and returns it
    def AskTable():
        table = input(bar + ".AskTable: What table? Type '-t'"
                      " for all table choices. ")      # Asks for table

        # Option to print all tables than asks again
        if table == "-t":
            query = ("SELECT TABLE_NAME, TABLE_COMMENT, TABLE_ROWS"
                     " FROM INFORMATION_SCHEMA.TABLES WHERE"
                     " TABLE_SCHEMA='manager'")

            table_list = _conn.Query("select", query)

            # Prints tables
            print("List of tables:")
            print(PrintSelect(table_list,
                              ["Table", "Description", "Entries"]))

            # Asks again
            table = input(bar + ": What table? ")

        # Makes sure the table exists
        query = ("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE"
                 " TABLE_SCHEMA='manager' AND TABLE_NAME='" +
                 table + "'")
        if not _conn.Query("select", query)[0][0]:
            print(bar + ": This table doesn't exit. You can create it"
                  " using the Create() function.")
            print()
            return ""
        else:
            print(bar + ": " + table + " table exists.")
            print()
            return table

    # Takes a 'join' list of format join[{:}][{3}]
    # and returns the corresponding query text
    # Example: join = [[$[1,1], $[1,2], $[1,3]], ...,
    # [$[n,1], $[n,2], $[n,3]]] becomes '...
    # JOIN $[i,1] ON $[i,2] = $[i,3] ...'
    def JoinText(_join):
        textjoin = ""
        if len(_join):
            if len(_join[0]) != 3:
                print("ERROR in " + bar +
                      ".JoinTable: Wrong number of arguments.")
            else:
                for j in _join:
                    textjoin += (" JOIN " + j[0] + " ON " +
                                 j[1] + " = " + j[2] + " ")
        return textjoin

    # Takes a 'where' list of format where[{:}][{2}]
    # and returns the corresponding query text
    # Example: where = [[$[1,1], $[1,2]], ..., [$[n,1], $[n,2]]] becomes
    # 'WHERE $[1,1] = $[1,2] AND ... AND $[i,1] = $[i,2] AND ...'
    def WhereText(_where):
        if len(_where):
            for i, w in enumerate(_where):
                if i == 0:
                    textwhere = "WHERE " + w
                else:
                    textwhere += " AND " + w
            return textwhere
        else:
            return ""
    # ######### End of the useful functions ##########

    bar = "MySQL.Do"
    print(bar + ": You want to perform an action on a table.")

    # ######### Initializing was is known from the function called ######
    # Table selection or table reading
    if _table == "":
        table = AskTable()
    else:
        table = _table

    if table == "":
        print(bar + ": No table was selected.")
    else:
        print(bar + ": You selected the " + table + " table.")

    # Join option reading
    if not len(_join):
        join = []
    else:
        join = _join
        print(bar + ": You have joined the " + str(join[:][0]) +
              " table(s).")

    # Where option reading
    where = []
    if len(_where):
        for w in _where:
            where.append(str(w[0]) + " = " + str(w[1]))
        print(bar + ": You added where constrains, " + str(where) + ".")

    # Action
    action = "?action"
    # ######### End of initializing was is knows ##########

    # ######### Starting the infinite loop ##########
    while True:
        print(bar + ": ####################")
        PrintOptions()
        args = input().split()

        # ######### Possible Options choosen ##########
        # Print the table list
        if args[0] == '-t':
            query = ("SELECT TABLE_NAME, TABLE_COMMENT, TABLE_ROWS FROM"
                     " INFORMATION_SCHEMA.TABLES WHERE"
                     " TABLE_SCHEMA='manager'")

            rows = _conn.Query("select", query)

            print(bar + ": List of tables.")
            print(PrintSelect(rows, ["Table", "Description", "Entries"]))

        # Print table desciptions
        elif args[0] == '-d':
            query = ("SELECT CONCAT(TABLE_NAME, '.', COLUMN_NAME)"
                     " as field, COLUMN_TYPE,"
                     " EXTRA FROM INFORMATION_SCHEMA."
                     " COLUMNS WHERE ")
            if len(args) > 1:
                query += "TABLE_NAME = '" + args[1] + "'"
                if len(args) > 2:
                    for t in range(2, len(args)):
                        query += " OR TABLE_NAME = '" + args[t] + "'"
            else:
                query += " TABLE_NAME = '" + table + "'"
                for j in join:
                    query += " OR TABLE_NAME = '" + j[0] + "'"

            rows = _conn.Query("select", query)

            print(bar + ": Desciption of the table(s)' fields:")
            print(PrintSelect(rows, ["Field", "Type", "Extra"]))

        # Join a table
        elif args[0] == '-j':
            if len(args) != 4:
                print("ERROR in " + bar +
                      ": Wrong amount of arguments for the join request.")
            else:
                textjoin = ""
                for j in range((len(args) - 1)/3):
                    join.append([args[3*j+1], args[3*j+2], args[3*j+3]])
                    textjoin += args[3*j+1] + " "
                print(bar + ".JoinTable: Joined table(s) " + textjoin +
                      "to the current table(s).")

        # Change a main table
        elif args[0] == "-c":
            new = args[1]
            query = ("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE"
                     " TABLE_SCHEMA='manager' AND TABLE_NAME='" + new +
                     "'")

            if not _conn.Query("select", query)[0][0]:
                print(bar + ": This table doesn't exit. Check the"
                      " available tables with -t. Keeping the previous"
                      " table, " + table + ".")
                print()
            else:
                print(bar + ": " + new + " table exists and was set as"
                      " new table.")
                print()
                table = new

        # Change the where conditions
        elif args[0] == "-w":
            where = []
            for i in range((len(args) - 1)/2):
                where.append(args[2*i+1] + " = " + args[2*i+2])
            print(bar + ": New WHERE constrains are " + str(where) + ".")

        # Select rows
        elif args[0] == '-s':
            fields = []
            if len(args) == 1 or args[1] == "*":
                textselect = "SELECT "
                where2 = "WHERE TABLE_NAME = '" + table + "'"
                for j in join:
                    where2 += " OR TABLE_NAME = '" + j[0] + "'"
                query = ("SELECT CONCAT(TABLE_NAME, '.', COLUMN_NAME) FROM"
                         " INFORMATION_SCHEMA.COLUMNS " + where2)
                raw_fields = _conn.Query("select", query)
                for i, f in enumerate(raw_fields):
                    fields.append(f[0])
                    if not i:
                        textselect += f[0]
                    else:
                        textselect += ", " + f[0]
            else:
                for i, s in enumerate(args):
                    if i:
                        fields.append(str(s))
                        if i == 1:
                            textselect = "SELECT " + str(s)
                        else:
                            textselect += ", " + str(s)
            rows = _conn.Query("select", textselect + " FROM " + table +
                               JoinText(join) + WhereText(where))

            print(bar + ": Selection asked.")
            print(PrintSelect(rows, fields))

            if input("Continue? (y/[n]) : ") != "y":
                break

        # Insert a rows
        elif args[0] == "-i":
            if len(join):
                print(bar + ": Cannot insert in a joined table!"
                      " Droping the join.")
                join = []
            query = ("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS"
                     " WHERE TABLE_NAME = '" + table + "' AND EXTRA <>"
                     " 'auto_increment'")
            raw_fields = _conn.Query("select", query)
            fields = []
            for f in raw_fields:
                fields.append(f[0])

            args = args[1:]

            if len(args) != len(fields) + 1:
                while len(args) != len(fields):
                    args = input(bar + ": To insert a row in " + table +
                                 ", you need to give the following"
                                 " values: "
                                 + str(fields) + ".\n\t").split()

            query = "INSERT INTO " + table + "("
            for i, f in enumerate(fields):
                if not i:
                    query += f
                else:
                    query += ", " + f
            query += ") VALUES ("
            for i, f in enumerate(args):
                if not i:
                    query += f
                else:
                    query += ", " + f
            query += ")"

            lastid = _conn.Query("insert", query)
            if isinstance(lastid, int):
                print(bar + ": Successfully inserted the row."
                      " The returned id is " + str(lastid) + ".")

            if input("Continue? (y/[n]) : ") != "y":
                break

        # Update rows
        elif args[0] == "-u":
            print(bar + ": TO DO")
#                if len(args) == 1:
#                    args = input()

        # Delete rows
        elif args[0] == "-r":
            print(bar + ": TO DO")
