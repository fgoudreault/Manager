import os
import re
import MySQL
from colorama import Fore, Back, Style
from copy import deepcopy

conn = MySQL.Connector(_side="briaree")
print("Base directory: ", conn.groundDir)


def SQLize_text(_string):
    new = ""
    for i in _string:
        if i == ".":
            new += "\\" + str(i)
        elif i == "+":
            new += "[\\" + str(i) + "]"
        else:
            new += str(i)
    return new


def FillTable(_type):
    if _type == "dmft":

        listdir = os.listdir(conn.groundDir)
        for dir in listdir:
            match = re.search('(^02)', dir)
            if match:
                dmftdir = dir + "/"

                print("Found a directory: " + dmftdir)
                args = []
                args.append(float(input("\tWhat is dmft U?\t")))
                args.append(float(input("\tWhat is dmft J?\t")))
                args.append(float(input("\tWhat is abinit U?\t")))
                args.append(float(input("\tWhat is abinit J?\t")))
                args.append(int(input("\tNumber of correlated orbitals?\t")))
                args.append(float(input("\tTemperature in electron-volt?\t")))

                listdir2 = next(os.walk("%s%s" % (conn.groundDir, dmftdir)))[1]
                for dir2 in listdir2:
                    if not re.match(r"^02\w?-[-_\+\w\.]+/\d{2,}-.+$", dir2):
                        continue
                    straindir = dmftdir + dir2 + "/"
                    query = ("SELECT COUNT(*) FROM dmft WHERE dmftdir='" +
                             straindir + "'")
                    if conn.Query("select", query)[0][0]:
                        print("Found a directory: " + straindir)
                        print("\tAlready in the database; skip")
                        continue

                    query = ("INSERT INTO dmft (dmftdir, dmftU, dmftJ,"
                             " abinitU, abinitJ, norb, temp, status) VALUES ('"
                             + straindir + "', %s, %s, %s, %s, %s, %s,"
                             " 'done')")
                    print("New entry in dmft table, id is %d" %
                          conn.Query("insert", query, tuple(args)))

    elif _type == "strain":

        dmfts = conn.Query("select", "SELECT dmftdir, dmftid FROM dmft")
        for dmft in dmfts:
            listdir = os.listdir(conn.groundDir + dmft[0])

            for dir in listdir:
                match = re.search(r'(^\d)', dir)

                if match:
                    straindir = "/" + dir
                    print("Found a directory: " + dmft[0] + straindir)

                    query = ("SELECT COUNT(*) FROM strain WHERE straindir='" +
                             straindir + "' AND dmftid=" + str(dmft[1]))
                    if conn.Query("select", query)[0][0]:
                        print("\t Already in the database; skip")
                    else:
                        args = [dmft[1], straindir]

                        query = ("INSERT INTO strain (dmftid, straindir)"
                                 " VALUES (%s, %s)")
                        conn.Query("execute", query, tuple(args))

    elif _type == "barechi1":
        listdir = next(os.walk(conn.groundDir))[1]
        for dir in listdir:
            match = re.search(r'(^04)(\w?)-', dir)
            if not match:
                continue

            dmftdir = dir + "/"
            print("\n%sEntering in %s%s%s" % (Style.BRIGHT, Style.RESET_ALL +
                                              Fore.GREEN, dmftdir, Fore.RESET))

            listdir2 = next(os.walk(conn.groundDir + dmftdir))[1]
            for dir2 in listdir2:
                if not re.match(r"^\d{2,}", dir2):
                    continue
                kdir = dmftdir + dir2 + "/"
                print("\n%sEntering in %s%s%s" % (Style.BRIGHT,
                                                  Style.RESET_ALL + Fore.GREEN,
                                                  kdir, Style.RESET_ALL))
                kgrid = input("\t%sWhat k-grid?%s\t" %
                              (Style.BRIGHT, Style.RESET_ALL))
                preargs = [[kgrid], ["kgrid"], ['t']]
                if kgrid == "":
                    continue

                for dir3 in os.listdir(conn.groundDir+kdir):
                    if not re.match(r"^\d{2,}", dir3):
                        continue
                    args = deepcopy(preargs)
                    straindir = kdir + dir3 + "/"

                    print("\n\t\t%sEntering in %s%s%s" % (Style.BRIGHT,
                                                          Style.RESET_ALL +
                                                          Fore.GREEN,
                                                          straindir,
                                                          Style.RESET_ALL))
                    m = re.match(r'^(\d+\w?)', dir3)
                    if m is None:
                        print("\t\t%sNot a calculation folder.%s" %
                              (Style.BRIGHT, Style.RESET_ALL))
                        continue

                    query = ("SELECT barechi1id FROM barechi1 WHERE"
                             " barechi1dir='" + straindir + "'")
                    select = conn.Query("select", query)
                    print(select)
                    if len(select):
                        print("\t\t%sAlready exists. barechi1id id %d%s" %
                              (Fore.BLUE, select[0][0], Style.RESET_ALL))
                        continue

                        # linked_strain = conn.Query("select", "SELECT
                        # straindir, strainid FROM strain WHERE dmftid=" +
                        # str(dmftid) + " AND straindir REGEXP '" +
                        # SQLize_text(m.group(0)) + "-.*'")
                        # if len(linked_strain) == 1:
                        #     print("\t\t%sKept calculation: straindir is %s\t
                        #           strainid is %d%s\t" % (Fore.MAGENTA,
                        #           linked_strain[0][0], linked_strain[0][1],
                        #           Style.RESET_ALL))
                        #     args[0].append(str(linked_strain[0][1]))
                        #     args[1].append("strainid")
                        #     args[2].append("i")
                        # else:
                        #     print("\t\t%sERROR: Count not wind the right
                        #           strain calculation!%s" % (Back.RED +
                        #           Fore.BLACK, Style.RESET_ALL))
                        #     continue
                    linked_dmft = conn.Query("select", "SELECT dmftdir, dmftid"
                                             " FROM dmft WHERE dmftdir REGEXP"
                                             "'^02%s-.+/%s-.*'" %
                                             (match.group(2), m.group(0)))
                    if len(linked_dmft) == 1:
                        print("\t\t%sKept calculation: straindir is %s\t"
                              " strainid is %d%s\t" %
                              (Fore.MAGENTA, linked_dmft[0][0],
                               linked_dmft[0][1], Style.RESET_ALL))
                        args[0].append(str(linked_dmft[0][1]))
                        args[1].append("dmftid")
                        args[2].append("i")
                    else:
                        print("\t\t%sERROR: Cound not find the right"
                              " strain calculation!%s" %
                              (Back.RED + Fore.BLACK, Style.RESET_ALL))
                        continue

                    args[0].append(straindir)
                    args[1].append("barechi1dir")
                    args[2].append("t")

                    args[0].append("done")
                    args[1].append("status")
                    args[2].append("t")

                    query = "INSERT INTO barechi1 ("
                    for a, arg in enumerate(args[1]):
                        if a == 0:
                            query += arg
                        else:
                            query += ', ' + arg
                    query += ") VALUES ("
                    for a, arg in enumerate(args[0]):
                        if args[2][a] == 't':
                            arg = "'" + arg + "'"
                        if not a:
                            query += arg
                        else:
                            query += ", " + arg
                    query += ")"
                    conn.Query("execute", query)

    elif _type == "barechi2":
        listdir = next(os.walk(conn.groundDir))[1]
        for dir in listdir:
            if re.search(r'(^04\w?)', dir):
                dmftdir = dir + "/"
                print("\n%sEntering in %s%s%s" %
                      (Style.BRIGHT, Style.RESET_ALL, Fore.GREEN, Fore.RESET))

                listdir2 = next(os.walk(conn.groundDir + dmftdir))[1]
                # listdir2 = os.listdir(conn.groundDir + dmftdir)
                for dir2 in listdir2:
                    match1 = re.search(r"(\d+)\w-", dir2)
                    if match1:
                        baredir = dmftdir + dir2 + "/"
                        print("\n\t%sEntering in %s%s%s" %
                              (Style.BRIGHT, Style.RESET_ALL + Fore.GREEN,
                               baredir, Style.RESET_ALL))

                        if conn.Query("select",
                                      "SELECT COUNT(*) FROM barechi1 WHERE"
                                      " barechi1dir REGEXP '^" +
                                      baredir + "'")[0][0]:
                            print("\t%s%s directory is used for type 1"
                                  " Barechi simulations.%s" %
                                  (Fore.RED, baredir, Fore.RESET))
                            continue

                        listdir3 = os.listdir(conn.groundDir + baredir)

                        n = 0
                        for dir3 in listdir3:
                            if re.match(r"^\d+-[-\.\+\w]+$", dir3):
                                n += 1
                        request = conn.Query("select",
                                             "SELECT COUNT(*) FROM barechi2"
                                             " WHERE barechi2dir REGEXP '^" +
                                             SQLize_text(baredir) + "'")
                        if request is None:
                            print("\t%sNothing to do here.%s" %
                                  (Fore.BLUE, Fore.RESET))
                            continue
                        number = request[0][0]
                        if number == n:
                            print("\t%sAlready full here.%s" %
                                  (Fore.BLUE, Fore.RESET))
                            continue
                        else:
                            print("\t%sThere are %s entries in the table with"
                                  " %s possibilities.%s" %
                                  (Fore.MAGENTA, str(number),
                                   str(n), Fore.RESET))

                        qpts = input("\t%sWhat are the qpts associated"
                                     " with this directory? %s\t" %
                                     (Style.BRIGHT, Style.RESET_ALL))
                        if qpts == "":
                            continue

                        for dir3 in listdir3:
                            barechi2dir = baredir + dir3 + "/"
                            print("\n\t\t%sEntering in %s%s%s" %
                                  (Style.BRIGHT, Style.RESET_ALL +
                                   Fore.GREEN,
                                   barechi2dir, Fore.RESET))
                            if conn.Query("select", "SELECT COUNT(*)"
                                          " FROM barechi2 WHERE"
                                          " barechi2dir = '" + barechi2dir +
                                          "'")[0][0]:
                                print("\t\t%s%s already exists.%s" %
                                      (Fore.YELLOW, barechi2dir, Fore.RESET))
                                continue
                            match = re.match(r"^(\d+\w?-[-\.\+\w]+/\d+)(\w?-[-"
                                             r"\w]+)(/\d+\w?-[-\.\+\w]+)/$",
                                             barechi2dir)
                            request = conn.Query("select", "SELECT barechi1id,"
                                                 " barechi1dir FROM barechi1"
                                                 " WHERE barechi1dir REGEXP"
                                                 " '^" +
                                                 SQLize_text(match.group(1)) +
                                                 ".+" +
                                                 SQLize_text(match.group(3)) +
                                                 "'")
                            if request is None:
                                print("\t\t%sNo corresponding barechi1"
                                      " calculation to %s%s" %
                                      (Fore.RED, barechi2dir, Fore.RESET))
                                continue
                            elif len(request) > 1:
                                print("\t\t%sCorresponding barechi1"
                                      " found: %s%s" %
                                      (Fore.RED, len(request), Fore.RESET))
                                continue
                            else:
                                print("\t\t%sKept calculation: barechi1dir"
                                      " is %s\t barechi1id is %s%s\t" %
                                      (Fore.MAGENTA, request[0][1],
                                       request[0][0], Fore.RESET))
                                args = [[request[0][0], barechi2dir,
                                         qpts, "done"],
                                        ["barechi1id", "barechi2dir",
                                         "qpts", "status"],
                                        ['i', 't', 't', "t"]]

                            if not os.path.isfile(conn.groundDir +
                                                  barechi2dir +
                                                  "/Input/Input.dat"):
                                print("\t\t%sEmpty directory!%s" %
                                      (Fore.RED, Fore.RESET))
                                continue

                            with open(conn.groundDir + barechi2dir +
                                      "/Input/Input.dat", "r") as f:
                                for i, line in enumerate(f):
                                    if i == 11:
                                        args[0].append(float(line.split()[0]))
                                        args[1].append("chempot")
                                        args[2].append("f")
                                    elif i == 14:
                                        args[0].append(int(line.split()[0]))
                                        args[1].append("niwn")
                                        args[2].append("i")
                                    elif i == 15:
                                        args[0].append(int(line.split()[0]))
                                        args[1].append("nivn")
                                        args[2].append("i")

                            query = "INSERT INTO barechi2 ("
                            for a, arg in enumerate(args[1]):
                                if a == 0:
                                    query += arg
                                else:
                                    query += ', ' + arg
                            query += ") VALUES ("
                            for a, arg in enumerate(args[0]):
                                if args[2][a] == 't':
                                    arg = "'" + str(arg) + "'"
                                if not a:
                                    query += str(arg)
                                else:
                                    query += ", " + str(arg)
                            query += ")"
                            conn.Query("execute", query)

    elif _type == "dressed":
        listdir = next(os.walk(conn.groundDir))[1]
        for dir in listdir:
            match = re.search(r"^06(\w?)-", dir)
            if match:
                dmftdir = dir + "/"
                print("\n%sEntering in %s%s%s" %
                      (Style.BRIGHT, Style.RESET_ALL +
                       Fore.GREEN, dmftdir, Fore.RESET))

                listdir2 = next(os.walk(conn.groundDir + dmftdir))[1]
                for dir2 in listdir2:
                    match2 = re.search(r"^(\d+\w?)-[-_\.\+\w]+$", dir2)
                    if match2:
                        barechidir = dmftdir + dir2 + "/"
                        print("\n\t%sEntering in %s%s%s" %
                              (Style.BRIGHT, Style.RESET_ALL + Fore.GREEN,
                               barechidir, Fore.RESET))

                        listdir3 = next(os.walk(conn.groundDir +
                                        barechidir))[1]
                        for dir3 in listdir3:
                            match3 = re.search(r"^(\d+\w?-[-_\.\+\w]+)$", dir3)
                            if match3:
                                dresseddir = barechidir + dir3 + "/"
                                print("\n\t\t%sEntering in %s%s%s" %
                                      (Style.BRIGHT, Style.RESET_ALL +
                                       Fore.GREEN, dresseddir, Fore.RESET))
                                s = (r"select", "SELECT barechi2dir, "
                                     r"barechi2id"
                                     r" FROM barechi2 WHERE barechi2dir REGEXP"
                                     r" '^04" + SQLize_text(match.group(1)) +
                                     r"-[-_\.\+a-zA-Z0-9]+/[0-9]+[a-zA-Z]?-[-_"
                                     r"\.\+a-zA-Z0-9]+/" +
                                     SQLize_text(match3.group(1)) + "/$'")
                                request = conn.Query(s)
                                if not len(request):
                                    input("\t\t\t%sNo barechi2 calculation"
                                          " could be matched!%s" %
                                          (Fore.RED, Fore.RESET))
                                    continue
                                print("\t\t\t%sMany possible calculations"
                                      " found:%s" % (Fore.BLUE, Fore.RESET))
                                print(MySQL.PrintSelect(request,
                                                        ["barechi2dir",
                                                         "barechi2id"]))

                                barechi2id = input("\t\t\t%sWhat id"
                                                   " corresponds?%s\t" %
                                                   (Style.BRIGHT,
                                                    Style.RESET_ALL))
                                ids = [x[1] for x in request]
                                if barechi2id == "":
                                    continue
                                elif not int(barechi2id) in ids:
                                    print("\t\t\t%sNot in the proposed list!"
                                          " Skipping." %
                                          (Fore.RED, Fore.RESET))
                                    continue
                                preargs = [[barechi2id, "done"],
                                           ['barechi2id', "status"],
                                           ['i', "t"]]

                                listdir4 = next(os.walk(conn.groundDir +
                                                        dresseddir))[1]
                                for dir4 in listdir4:
                                    match4 = re.match(r"^U-([\.\d]+)$", dir4)
                                    if not match4:
                                        continue

                                    dressedU = match4.group(1)
                                    listdir5 = next(os.walk(conn.groundDir +
                                                            dresseddir +
                                                            dir4))[1]
                                    for dir5 in listdir5:
                                        match5 = re.match(r"^J-([\.\d]+)$",
                                                          dir5)
                                        if not match5:
                                            continue
                                        fulldir = (dresseddir + dir4 + "/" +
                                                   dir5 + "/")

                                        print("\n\t\t\t\t%sEntering in"
                                              " %s%s%s" %
                                              (Style.BRIGHT, Style.RESET_ALL +
                                               Fore.GREEN, fulldir,
                                               Fore.RESET))

                                        args = deepcopy(preargs)
                                        dressedJ = match5.group(1)
                                        args[0].append(fulldir)
                                        args[0].append(dressedU)
                                        args[0].append(dressedJ)
                                        args[1].append("dresseddir")
                                        args[1].append("dressedU")
                                        args[1].append("dressedJ")
                                        args[2].append("t")
                                        args[2].append("f")
                                        args[2].append("f")
                                        f = (conn.groundDir + fulldir +
                                             "Input/Input.dat")
                                        if not os.path.isfile(f):
                                            print("\t\t\t\t%sNo Input.dat"
                                                  " file!" %
                                                  (Fore.RED, Fore.RESET))
                                            continue

                                        with open(conn.groundDir + fulldir +
                                                  "Input/Input.dat") as f:
                                            for i, line in enumerate(f):
                                                s = line.split()
                                                if i == 8:
                                                    args[0].append(s[0])
                                                    args[1].append("miwn")
                                                    args[2].append("i")
                                                elif i == 9:
                                                    args[0].append(s[0])
                                                    args[1].append("mivn")
                                                    args[2].append("i")
                                        s = ("SELECT COUNT(*) FROM dressed"
                                             " WHERE dresseddir = '" +
                                             dresseddir +
                                             "' AND ABS(dressedU - " +
                                             str(dressedU) +
                                             ") < 0.0001 AND ABS(dressedJ - " +
                                             str(dressedJ) + ") < 0.0001")
                                        request = conn.Query("select", s)
                                        if request[0][0]:
                                            print("\t\t\t\t%sThis dressed"
                                                  " calculation is already in"
                                                  " the database." %
                                                  (Fore.BLUE, Fore.RESET))
                                            continue

                                        query = "INSERT INTO dressed ("
                                        for a, arg in enumerate(args[1]):
                                            if a == 0:
                                                query += arg
                                            else:
                                                query += ', ' + arg
                                        query += ") VALUES ("
                                        for a, arg in enumerate(args[0]):
                                            if args[2][a] == 't':
                                                arg = "'" + str(arg) + "'"
                                            if not a:
                                                query += str(arg)
                                            else:
                                                query += ", " + str(arg)
                                        query += ")"
                                        conn.Query("execute", query)

    elif _type == "eliash":
        listdir = os.listdir(conn.groundDir)
        for dir in listdir:
            match = re.search(r"^07(\w?)-", dir)
            if not match:
                continue
            dmftdir = dir + "/"
            print("\n%sEntering in %s%s%s" % (Style.BRIGHT, Style.RESET_ALL +
                                              Fore.GREEN, dmftdir, Fore.RESET))

            listdir2 = next(os.walk(conn.groundDir + dmftdir))[1]
            for dir2 in listdir2:
                match2 = re.search(r"^(\d+\w?)-[-_\.\+\w]+$", dir2)
                if not match2:
                    continue
                baredir = dmftdir + dir2 + "/"
                print("\n\t%sEntering in %s%s%s" %
                      (Style.BRIGHT, Style.RESET_ALL + Fore.GREEN,
                       baredir, Fore.RESET))
                request = conn.Query(r"select", "SELECT dresseddir FROM"
                                     r" dressed WHERE dresseddir REGEXP '^06"
                                     + match.group(1) +
                                     r"-[-_\.\+a-zA-Z0-9]+/'")
                if not request:
                    continue

                subdir = []
                i = 0
                for r in request:
                    sub = re.match(r"^(06\w?-[-_\.\+\w]+/\d+\w?"
                                   r"-[-_\.\+\w]+/).+", r[0]).group(1)
                    if not len(subdir) or sub not in [s[0] for s in subdir]:
                        subdir.append([sub, i])
                        i += 1
                print("\t%sPossible dressedchi folders related"
                      " to these eliashberg:?%s" % (Fore.BLUE, Fore.RESET))
                print(MySQL.PrintSelect(subdir, ["dir", "id"]))
                id = input("\t%sWhich one is it? Put the id:%s\t" %
                           (Style.BRIGHT, Style.RESET_ALL))
                if id == "":
                    continue
                id = int(id)

                if int(id) > i:
                    print("\t%sId not in the choices!%s" %
                          (Fore.RED, Fore, Fore.RESET))
                    continue

                listdir3 = next(os.walk(conn.groundDir + baredir))[1]
                for dir3 in listdir3:
                    if not re.match(r"^\d+-[-_\.\+\w]+$", dir3):
                        continue
                    straindir = baredir + dir3 + "/"
                    print("\n\t\t%sEntering in %s%s%s" %
                          (Style.BRIGHT, Style.RESET_ALL + Fore.GREEN,
                           straindir, Fore.RESET))

                    listUdir = next(os.walk(conn.groundDir + straindir))[1]
                    print(listUdir)
                    for Udir in listUdir:
                        match4 = re.match(r"^U-(\d+\.\d+)$", Udir)

                        if not match4:
                            continue

                        U = round(float(match4.group(1)), 2)
                        sss = "{0}{1}{2}".format(conn.groundDir, straindir,
                                                 Udir)
                        listdirJ = next(os.walk(sss))[1]
                        for dirJ in listdirJ:
                            match5 = re.match(r"^J-([\d\.]+)$", dirJ)

                            if not match5:
                                continue

                            J = round(float(match5.group(1)), 2)

                            preeliashdir = straindir + Udir + "/" + dirJ + "/"
                            ss = ("\n\t\t\t{0}Entering"
                                  " in {1}{2}{3}{4}".format(Style.BRIGHT,
                                                            Style.RESET_ALL,
                                                            Fore.GREEN,
                                                            preeliashdir,
                                                            Fore.RESET))
                            print(ss)
                            ss = ("\t\t\t\t{0}(U, J) is ({1:.2f},"
                                  "{2:.2f}){3}".format(Fore.MAGENTA, U, J,
                                                       Fore.RESET))
                            print(ss)

                            request = conn.Query("select", "SELECT dresseddir,"
                                                 " dressedU, dressedJ,"
                                                 " dressedid"
                                                 " FROM dressed WHERE"
                                                 " dresseddir REGEXP '^" +
                                                 SQLize_text(subdir[id][0]) +
                                                 SQLize_text(dir3) +
                                                 "/' AND ABS(dressedU - " +
                                                 str(U) +
                                                 ") < 0.001 AND "
                                                 "ABS(dressedJ - " +
                                                 str(J) + ") < 0.001")

                            if not len(request) == 1:
                                ss = ("\n\t\t\t\t{0}Couldn't find the"
                                      " corresponding dressed calculation!{1}")
                                print(ss.format(Fore.RED, Fore.RESET))
                                continue

                            request = request[0]
                            print("\n\t\t\t\t{0}Found a corresponding dressed"
                                  " calculation: U is {1}, J is {2}, located"
                                  " at {3} with id"
                                  " {4}{5}".format(Fore.YELLOW,
                                                   request[1],
                                                   request[2],
                                                   request[0],
                                                   request[3],
                                                   Style.RESET_ALL))
                            dressedid = request[3]
                            xx = os.walk("{0}{1}".format(conn.groundDir,
                                                         preeliashdir))
                            listdirParity = next(xx)[1]
                            for dirParity in listdirParity:
                                no_s = dirParity != "Singlet"
                                no_t = dirParity != "Triplet"
                                if no_s and no_t:
                                    continue
                                if dirParity == "Singlet":
                                    parity = 0
                                elif dirParity == "Triplet":
                                    parity = 1
                                eliashdir = preeliashdir + dirParity + "/"

                                print("\n\t\t\t\t\t{0}Parity :"
                                      " {1}{2}".format(Fore.MAGENTA, dirParity,
                                                       Fore.RESET))
                                args = [[dressedid, parity, eliashdir],
                                        ["dressedid", "parity", "eliashdir"],
                                        ["i", "t", "t"]]

                                # request = conn.Query("select", "SELECT
                                # COUNT(*) FROM eliash WHERE eliashdir = '{0}'
                                # AND parity = '{1}' AND dressedid = {2}".
                                # format(eliashdir, parity, dressedid))[0][0]
                                # if request:
                                #     print("\t\t\t\t\t{0}This calculation is
                                # already in the database.{1}".
                                # format(Fore.BLUE, Fore.RESET))
                                #     continue

                                if not os.path.isfile("{0}{1}/Input/Input.dat"
                                                      "".format(conn.groundDir,
                                                                eliashdir)):
                                    print("\t\t\t\t\t{0}No Input.dat file!"
                                          "{1}".format(Fore.RED, Fore.RESET))
                                    continue

                                with open("{0}{1}/Input/Input."
                                          "dat".format(conn.groundDir,
                                                       eliashdir)) as f:
                                    for i, line in enumerate(f):
                                        if i == 8:
                                            livn = [int(line.split()[0]),
                                                    "livn", "i"]
                                            args = [x + [livn[i]] for i, x in
                                                    enumerate(args)]
                                        elif i == 9:
                                            liwn = [int(line.split()[0]),
                                                    "liwn", "i"]
                                            args = [x + [liwn[i]] for i, x in
                                                    enumerate(args)]
                                        elif i == 11:
                                            liwn_max = [int(line.split()[0]),
                                                        "liwn_max", "i"]
                                            args = [x + [liwn_max[i]] for i, x
                                                    in enumerate(args)]
                                        elif i == 13:
                                            if parity != int(line.split()[0]):
                                                print("\t\t\t\t\t{0}Parity in"
                                                      " the file different"
                                                      " than in the path!"
                                                      "{1}".format(Fore.RED,
                                                                   Fore.RESET))
                                        elif i == 16:
                                            nvec = [int(line.split()[0]),
                                                    "nvec", "i"]
                                            args = [x + [nvec[i]] for i, x in
                                                    enumerate(args)]

                                query = "INSERT INTO eliash ("
                                for a, arg in enumerate(args[1]):
                                    if a == 0:
                                        query += arg
                                    else:
                                        query += ', ' + arg
                                query += ") VALUES ("
                                for a, arg in enumerate(args[0]):
                                    if args[2][a] == 't':
                                        arg = "'" + str(arg) + "'"
                                    if not a:
                                        query += str(arg)
                                    else:
                                        query += ", " + str(arg)
                                query += ")"
                                conn.Query("execute", query)
