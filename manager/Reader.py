import pylab as pl


class SettingsReader:

    def __init__(self, sfile):
        # Default variables
        # ____________________
        # INPUT FILES
        # Eigenvalues
        self.eig_file = ''
        # Projectors
        self.proj_file = ''
        # K points
        self.kpt_file = ''
        # Real axis Self-energy
        self.Sw_files = []
        # OUTPUT FILES
        self.out_file = 'Outfile'

        # SYSTEM PARAMETERS
        # System name
        self.sysname = 'System'
        # Fermi energy
        self.fermi = 0
        # K points
        self.nkpt = 0
        self.nkptx = 0
        self.nkpty = 0
        self.nkptz = 0
        # Bands
        self.nband = 0
        self.band0 = 1
        self.bands = []
        # Orbitals
        self.norb = 0
        # Translational vectors in reciprocal space
        self.B = pl.matrix(((1, 0, 0), (0, 1, 0), (0, 0, 1)))

        # PLOTING FIGURE PARAMETERS
        # Figure size
        self.figsize = [8, 8]
        # Axis ticks
        self.xticks = []
        self.yticks = []
        # Axis labels
        self.xlabel = []
        self.ylabel = []

        # From the settings file, set variables
        # ____________________

        with open(sfile) as f:
            _settings = []
            for lines in f:
                _settings.append(lines.split())

        for s in _settings:
            if len(s) > 0:
                # INPUT FILES
                # Eingenvalues
                if s[0] == 'eig_file':
                    self.eig_file = s[1]
                # Projectors
                elif s[0] == 'proj_file':
                    self.proj_file = s[1]
                # K points
                elif s[0] == 'kpt_file':
                    self.kpt_file = s[1]
                # Real axis Self-energies
                elif s[0] == 'Sw_files':
                    for i in range(1, len(s)):
                        self.Sw_files.append(s[i])

                # OUTPUT FILES
                elif s[0] == 'out_file':
                    self.out_file = s[1]

                # SYSTEM PARAMETERS
                # System name
                elif s[0] == 'sysname':
                    self.sysname = s[1]
                # Fermi energy
                elif s[0] == 'fermi':
                    self.fermi = float(s[1])
                # K points
                elif s[0] == 'nkpt':
                    self.nkpt = int(s[1])
                elif s[0] == 'nkptx':
                    self.nkptx = int(s[1])
                elif s[0] == 'nkpty':
                    self.nkpty = int(s[1])
                elif s[0] == 'nkptz':
                    self.nkptz = int(s[1])
                # Bands
                elif s[0] == 'nband':
                    self.nband = int(s[1])
                elif s[0] == 'band0':
                    self.band0 = int(s[1])
                elif s[0] == 'bands':
                    for i in range(1, len(s)):
                        self.bands.append(int(s[i]))
                # Orbs
                elif s[0] == 'norb':
                    self.norb = int(s[1])
                # Translational vectors in reciprocal space
                elif s[0] == 'b':
                    if len(s) != 10:
                        print('Setting ERROR: B (matrix to change basis to'
                              ' reciprocal vector space) needs 9 components;'
                              'Taken to be the identity.')
                    else:
                        arr = ((float(s[1]), float(s[2]), float(s[3])),
                               (float(s[4]), float(s[5]), float(s[6])),
                               (float(s[7]), float(s[8]), float(s[9])))
                        self.B = pl.matrix(arr)
                # Self-energy
                elif s[0] == 'use_self':
                    self.use_self = int(s[1])

                # PLOTING FIGURES PARAMETERS
                # Figure size
                elif s[0] == 'figsize':
                    if len(s) != 3:
                        print('Setting ERROR: figure size settings incorrect,'
                              ' needs 2 components; Taken to be 8 8.')
                    else:
                        self.figsize = [float(s[1]), float(s[2])]
                # Ticks
                elif s[0] == 'xticks':
                    if s[1] == 'str':
                        self.xticks = [[], []]
                        for i in range((len(s) - 2)/2):
                            self.xticks[0].append(float(s[2*(i+1)]))
                            self.xticks[1].append(s[2*i+3])
                    elif s[1] == 'num':
                        print('xticks num: to do')
                    else:
                        print(r'Setting ERROR: xticks have 2 options, str or'
                              r' num. Ex1: xticks str 0 $\Gamma$ 12 $\chi$.'
                              r' Ex2: xticks num 0.5 @ 2.5 0.5.')
                elif s[0] == 'yticks':
                    if s[1] == 'str':
                        self.yticks = [[], []]
                        for i in range((len(s) - 2)/2):
                            self.yticks[0].append(float(s[2*(i+1)]))
                            self.yticks[1].append(s[2*i+3])
                    elif s[1] == 'num':
                        print('yticks num: to do')
                    else:
                        print(r'Setting ERROR: yticks have 2 options,'
                              r' str or num. Ex1: yticks str 0 $\Gamma$ 12'
                              r' $\chi$. Ex2: yticks num 0.5 @ 2.5 0.5.')
                # Labels
                elif s[0] == 'xlabel':
                    self.xlabel = s[1]
                elif s[0] == 'ylabel':
                    self.ylabel = s[1]
                # Broadening
                elif s[0] == 'broad':
                    self.broad = float(s[1])

                # PLOTING OPTIONS
                # With add dashed line to highlight the
                # Brillouin Zone. 1: Used for diamond shape.
                elif s[0] == 'BZopt':
                    self.BZopt = int(s[1])
