from configparser import ConfigParser
import os
import re


def read_db_config(filename='/RQexec/gingras1/Database/config.ini',
                   section='duplessis'):
    """ Read database configuration file and return a dictionary object
    :param filename: name of the configuration file
    :param section: section of database configuration
    :return: a dictionary of database parameters
    """

    filePath = re.match(r"^(.+/)\w+\.\w+$",
                        os.path.realpath(__file__)).group(1)
    filename = "%s/config.ini" % filePath

    # if section == "briaree":
    #     filename = "/RQexec/gingras1/PythonCodes/Managers/MySQL/config.ini"
    # elif section == "mp2":
    #     filename = ("/mnt/parallel_scratch_mp2_wipe_on_december_2017/"
    #                 "tremblay/gingras1/Codes/Manager/manager/MySQL/config.ini")

    # Create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(filename)

    # Get section, default to mysql
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception("ERROR read_db_config: {0} not found "
                        "in the {1} file.".format(section, filename))

    return db
