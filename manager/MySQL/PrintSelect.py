import colorama
from colorama import Fore


def PrintSelect(select, fields):
    bar = "MySQL.PrintSelect"
    colorama.init()

    if not len(select):
        print(bar + ": EMPTY SELECT.")
        return ""
    else:
        if len(select[0]) != len(fields):
            print("ERROR in " + bar +
                  ": Requieres matching length of select"
                  " and fields arguments.")
            return ""
        else:
            text = Fore.BLUE
            for i in range(len(select)):
                text += "!"
                for j in range(len(select[0])):
                    text += ("\t" + str(fields[j]) + ":\t" +
                             str(select[i][j]) + ";")
                text += "\n"
            return text + Fore.RESET
